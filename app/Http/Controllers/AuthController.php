<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        if ($user = Auth::user()) {
            if($user->level == 'kades') {
                return redirect()->intended('dashboard');
            } elseif ($user->level == 'staff') {
                return redirect()->intended('dashboard');
            }
        }


        if($request->post())
        {
            request()->validate(
            [
                'username' => 'required',
                'password' => 'required',
            ]);

        $kredensil = $request->only('username','password');
        // dd(Auth::user());
        if (Auth::attempt($kredensil)) {
            $user = Auth::user();
                if ($user->level == 'kades') {
                    return redirect()->intended('dashboard');
                } elseif ($user->level == 'staff') {
                    return redirect()->intended('dashboard');
                }
                return redirect()->intended('/');
            }

        return redirect('login')->withInput()->withErrors(['error' => 'Username atau Password salah!!!']);

        }
        return view('auth');
        
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return Redirect('login');
    }
}
