<?php

namespace App\Http\Controllers;

use App\Imports\KKImport;
use App\Models\Banjar;
use App\Models\KK;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class DataKKController extends Controller
{
    public function index(Request $request)
    {
        if(isset($request->add)){
            // dd($request->post('kode_keluarga'));
            $validator = Validator::make($request->all(),[
                'alamat' => 'required',
                'kode_keluarga'=> 'required|numeric',
                'nama_kepala_keluarga'=> 'required',
                'no'=> 'required|numeric',
                'nik'=> 'required|numeric' ,
                // 'nama_anggota_keluarga'=> 'required',
                'jenis_kelamin'=> 'required',
                'hubungan'=> 'required',
                'tempat_lahir'=> 'required',
                'tanggal_lahir'=> 'required',
                'usia'=> 'required',
                'status'=> 'required',
                'agama'=> 'required',
                // 'golongan_darah'=> 'required',
                // 'kewarganegaraan'=> 'required',
                'pendidikan'=> 'required',
                'pekerjaan'=> 'required',
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            $kk = [
                'nama_kk'=>$request->post('nama_kk'),
                'no_kk'=>$request->post('no_kk'),
                'nik'=>$request->post('nik'),
                'alamat'=>$request->post('alamat'),
                'kode_keluarga'=> $request->post('kode_keluarga'),
                'nama_kepala_keluarga'=> $request->post('nama_kepala_keluarga'),
                'no'=> $request->post('no'),
                'nik'=> $request->post('nik'),
                'nama_anggota_keluarga'=> $request->post('nama_kepala_keluarga'),
                'jenis_kelamin'=> $request->post('jenis_kelamin'),
                'hubungan'=> $request->post('hubungan'),
                'tempat_lahir'=> $request->post('tempat_lahir'),
                'tanggal_lahir'=> $request->post('tanggal_lahir'),
                'usia'=> $request->post('usia'),
                'status'=> $request->post('status'),
                'agama'=> $request->post('agama'),
                'golongan_darah'=> 'Tidak Tahu',
                'kewarganegaraan'=> 'Warga Negara Indonesia',
                'pendidikan'=> $request->post('pendidikan'),
                'pekerjaan'=> $request->post('pekerjaan'),
            ];

            KK::create($kk);
            return back()->with('success','Data Berhasil Ditambah');
        }

        if(isset($request->edit)){
            $validator = Validator::make($request->all(),[
                'alamat' => 'required',
                'kode_keluarga'=> 'required|numeric|max:16',
                'nama_kepala_keluarga'=> 'required',
                'no'=> 'required|numeric',
                'nik'=> 'required|numeric|max:16' ,
                'nama_anggota_keluarga'=> 'required',
                'jenis_kelamin'=> 'required',
                'hubungan'=> 'required',
                'tempat_lahir'=> 'required',
                'tanggal_lahir'=> 'required',
                'usia'=> 'required',
                'status'=> 'required',
                'agama'=> 'required',
                'golongan_darah'=> 'required',
                'kewarganegaraan'=> 'required',
                'pendidikan'=> 'required',
                'pekerjaan'=> 'required',
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            $kk = [
                'nama_kk'=>$request->post('nama_kk'),
                'no_kk'=>$request->post('no_kk'),
                'nik'=>$request->post('nik'),
                'alamat'=>$request->post('alamat'),
                'kode_keluarga'=> $request->post('kode_keluarga'),
                'nama_kepala_keluarga'=> $request->post('nama_kepala_keluarga'),
                'no'=> $request->post('no'),
                'nik'=> $request->post('nik'),
                'nama_anggota_keluarga'=> $request->post('nama_anggota_keluarga'),
                'jenis_kelamin'=> $request->post('jenis_kelamin'),
                'hubungan'=> $request->post('hubungan'),
                'tempat_lahir'=> $request->post('tempat_lahir'),
                'tanggal_lahir'=> $request->post('tanggal_lahir'),
                'usia'=> $request->post('usia'),
                'status'=> $request->post('status'),
                'agama'=> $request->post('agama'),
                'golongan_darah'=> $request->post('golongan_darah'),
                'kewarganegaraan'=> $request->post('kewarganegaraan'),
                'pendidikan'=> $request->post('pendidikan'),
                'pekerjaan'=> $request->post('pekerjaan'),
            ];

            KK::where('id',$request->id)->update($kk);
            return back()->with('success','Data Berhasil Diedit');
        }

        if(isset($request->import)){
            if(empty($request->file)){
                return back()->with('info','Data Gagal Diimport');
            }
            ini_set('max_execution_time', 600);
            Excel::import(new KKImport,request()->file('file'));
            return back()->with('success','Data Berhasil Diimport');

        }

        if(isset($request->truncate)){
            KK::truncate();
            return back()->with('success','Data Berhasil Dikosongkan');

        }

        
        DB::statement("SET SQL_MODE=''");
        return view('data-kk.data-kk',[
            'title'=>'Data KK',
            // 'kk'=> DB::table('datakk')->select('datakk.*','banjar.nama_banjar')->join('banjar','banjar.id','=','datakk.alamat')->get(),
            'kk'=> DB::table('datakk')->select('nama_kepala_keluarga','kode_keluarga','nik','alamat','id')->groupBy('kode_keluarga')->get(),

        ]);
    }

    public function detail(Request $request, $id)
    {
        if(isset($request->add)){
            $validator = Validator::make($request->all(),[
                'alamat' => 'required',
                // 'kode_keluarga'=> 'required',
                // 'nama_kepala_keluarga'=> 'required',
                'no'=> 'required|numeric',
                'nik'=> 'required' ,
                'nama_anggota_keluarga'=> 'required',
                'jenis_kelamin'=> 'required',
                'hubungan'=> 'required',
                'tempat_lahir'=> 'required',
                'tanggal_lahir'=> 'required',
                'usia'=> 'required',
                'status'=> 'required',
                'agama'=> 'required',
                // 'golongan_darah'=> 'Tidak Tahu',
                // 'kewarganegaraan'=> 'Warga Negara Indonesia',
                'pendidikan'=> 'required',
                'pekerjaan'=> 'required',
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            $kk = [
                'kode_keluarga'=>KK::where('kode_keluarga',$id)->first()->kode_keluarga,
                'nik'=>$request->post('nik'),
                'alamat'=>$request->post('alamat'),
                'nama_kepala_keluarga'=> KK::where('kode_keluarga',$id)->first()->nama_kepala_keluarga,
                'no'=> $request->post('no'),
                'nik'=> $request->post('nik'),
                'nama_anggota_keluarga'=> $request->post('nama_anggota_keluarga'),
                'jenis_kelamin'=> $request->post('jenis_kelamin'),
                'hubungan'=> $request->post('hubungan'),
                'tempat_lahir'=> $request->post('tempat_lahir'),
                'tanggal_lahir'=> $request->post('tanggal_lahir'),
                'usia'=> $request->post('usia'),
                'status'=> $request->post('status'),
                'agama'=> $request->post('agama'),
                'golongan_darah'=> 'Tidak Tahu',
                'kewarganegaraan'=> 'Warga Negara Indonesia',
                'pendidikan'=> $request->post('pendidikan'),
                'pekerjaan'=> $request->post('pekerjaan'),
            ];

            // dd($kk);
            KK::create($kk);
            return back()->with('success','Data Berhasil Ditambah');
        }

        if(isset($request->edit)){
            $validator = Validator::make($request->all(),[
                'alamat' => 'required',
                // 'kode_keluarga'=> 'required',
                // 'nama_kepala_keluarga'=> 'required',
                'no'=> 'required|numeric',
                'nik'=> 'required' ,
                'nama_anggota_keluarga'=> 'required',
                'jenis_kelamin'=> 'required',
                'hubungan'=> 'required',
                'tempat_lahir'=> 'required',
                'tanggal_lahir'=> 'required',
                'usia'=> 'required',
                'status'=> 'required',
                'agama'=> 'required',
                // 'golongan_darah'=> 'Tidak Tahu',
                // 'kewarganegaraan'=> 'Warga Negara Indonesia',
                'pendidikan'=> 'required',
                'pekerjaan'=> 'required',
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            $kk = [
                'kode_keluarga'=>KK::where('kode_keluarga',$id)->first()->kode_keluarga,
                'nik'=>$request->post('nik'),
                'alamat'=>$request->post('alamat'),
                'nama_kepala_keluarga'=> KK::where('kode_keluarga',$id)->first()->nama_kepala_keluarga,
                'no'=> $request->post('no'),
                'nik'=> $request->post('nik'),
                'nama_anggota_keluarga'=> $request->post('nama_anggota_keluarga'),
                'jenis_kelamin'=> $request->post('jenis_kelamin'),
                'hubungan'=> $request->post('hubungan'),
                'tempat_lahir'=> $request->post('tempat_lahir'),
                'tanggal_lahir'=> $request->post('tanggal_lahir'),
                'usia'=> $request->post('usia'),
                'status'=> $request->post('status'),
                'agama'=> $request->post('agama'),
                'golongan_darah'=> 'Tidak Tahu',
                'kewarganegaraan'=> 'Warga Negara Indonesia',
                'pendidikan'=> $request->post('pendidikan'),
                'pekerjaan'=> $request->post('pekerjaan'),
            ];

            KK::where('id',$request->id)->update($kk);
            return back()->with('success','Data Berhasil Diedit');
        }
        
        DB::statement("SET SQL_MODE=''");
        return view('data-kk.data-kk-detail',[
            'title'=>'Data KK Detail',
            'kk'=> DB::table('datakk')->select('hubungan','nama_anggota_keluarga','nama_kepala_keluarga','kode_keluarga','nik','alamat','id')->where('kode_keluarga',$id)->get(),
            'detail'=>DB::table('datakk')->select('kode_keluarga','nama_kepala_keluarga')->where('kode_keluarga',$id)->first(),

        ]);
    }

    public function edit($id)
    {
        $kk = KK::find($id);
        return response()->json([
            'status'=> 200,
            'kk' => $kk
        ]);
    }
  

    public function destroy(Request $request)
    {
        KK::where('kode_keluarga',$request->id)->delete();
        return back()->with('success','Data Berhasil Dihapus');
    }
}
