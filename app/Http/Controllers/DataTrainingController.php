<?php

namespace App\Http\Controllers;

use App\Imports\DataTrainingImport;
use App\Models\DataTraining;
use App\Models\KK;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class DataTrainingController extends Controller
{
    public function index(Request $request)
    {
        if(isset($request->add)){
            // dd($request->miskin_ekstrim);
            $validator = Validator::make($request->all(),[
                'nik'=>'required|unique:App\Models\DataTraining,nik',
                // 'miskin_ekstrim'=>'required',
                // 'mata_pencaharian'=>'required',
                // 'sakit_menahun'=>'required',
                // 'keluarga_miskin'=>'required',
                // 'covid'=>'required',
                // 'lanjut_usia'=>'required',
                'blt'=>'required',
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            if($request->miskin_ekstrim=='Ya'){
                $miskin_ekstrim = 'Ya';
            }else{
                $miskin_ekstrim='Tidak';
            }
            if($request->mata_pencaharian=='Ya'){
                $mata_pencaharian = 'Ya';
            }else{
                $mata_pencaharian='Tidak';
            }
            if($request->sakit_menahun=='Ya'){
                $sakit_menahun = 'Ya';
            }else{
                $sakit_menahun='Tidak';
            }
            if($request->keluarga_miskin=='Ya'){
                $keluarga_miskin = 'Ya';
            }else{
                $keluarga_miskin='Tidak';
            }
            if($request->covid=='Ya'){
                $covid = 'Ya';
            }else{
                $covid='Tidak';
            }
            if($request->lanjut_usia=='Ya'){
                $lanjut_usia = 'Ya';
            }else{
                $lanjut_usia='Tidak';
            }

            $datakk = DB::table('datakk')->select('nama_anggota_keluarga','kode_keluarga')->where('nik',$request->nik)->first();
            $cek = DataTraining::where('kode_keluarga',$datakk->kode_keluarga)->count();
            // dd($cek);
            if($cek>0){
                return back()->with('info','Data Gagal Ditambah, NIK dalam KK ini sudah terdaftar!');
            }
            $training = [
                'kode_keluarga'=>$datakk->kode_keluarga,
                'nik'=>$request->nik,
                'nama_anggota_keluarga'=>$datakk->nama_anggota_keluarga,
                'miskin_ekstrim'=>$miskin_ekstrim,
                'mata_pencaharian'=>$mata_pencaharian,
                'sakit_menahun'=>$sakit_menahun,
                'keluarga_miskin'=>$keluarga_miskin,
                'covid'=>$covid,
                'lanjut_usia'=>$lanjut_usia,
                'blt'=>$request->post('blt'),
            ];

            DataTraining::create($training);
            return back()->with('success','Data Berhasil Ditambah');
        }

        if(isset($request->edit)){
            $validator = Validator::make($request->all(),[
                // 'kode_keluarga'=>'required',
                // 'miskin_ekstrim'=>'required',
                // 'mata_pencaharian'=>'required',
                // 'sakit_menahun'=>'required',
                // 'keluarga_miskin'=>'required',
                // 'covid'=>'required',
                // 'lanjut_usia'=>'required',
                'blt'=>'required',
            ]);

            if ($validator->fails()) {
                return back()->withInput()->withErrors($validator);
            }

            if($request->miskin_ekstrim=='Ya'){
                $miskin_ekstrim = 'Ya';
            }else{
                $miskin_ekstrim='Tidak';
            }
            if($request->mata_pencaharian=='Ya'){
                $mata_pencaharian = 'Ya';
            }else{
                $mata_pencaharian='Tidak';
            }
            if($request->sakit_menahun=='Ya'){
                $sakit_menahun = 'Ya';
            }else{
                $sakit_menahun='Tidak';
            }
            if($request->keluarga_miskin=='Ya'){
                $keluarga_miskin = 'Ya';
            }else{
                $keluarga_miskin='Tidak';
            }
            if($request->covid=='Ya'){
                $covid = 'Ya';
            }else{
                $covid='Tidak';
            }
            if($request->lanjut_usia=='Ya'){
                $lanjut_usia = 'Ya';
            }else{
                $lanjut_usia='Tidak';
            }

            $training = [
                // 'kode_keluarga'=>$request->post('kode_keluarga'),
                'miskin_ekstrim'=>$miskin_ekstrim,
                'mata_pencaharian'=>$mata_pencaharian,
                'sakit_menahun'=>$sakit_menahun,
                'keluarga_miskin'=>$keluarga_miskin,
                'covid'=>$covid,
                'lanjut_usia'=>$lanjut_usia,
                'blt'=>$request->post('blt'),
            ];

            DataTraining::where('id',$request->id)->update($training);
            return back()->with('success','Data Berhasil Diubah');
        }

        if(isset($request->import)){
            if(empty($request->file))
            {
                return back()->with('info','Data Gagal Diimport');  
            }
            ini_set('max_execution_time', 600);
            Excel::import(new DataTrainingImport,request()->file('file'));
            return back()->with('success','Data Berhasil Diimport');

        }

        if(isset($request->truncate)){
            DataTraining::truncate();
            return back()->with('success','Data Berhasil Dikosongkan');

        }

        $kk = KK::select('nik','usia')->get();
        foreach($kk as $k){
            if($k->usia>=60){
                DataTraining::where('nik',$k->nik)->update(['lanjut_usia'=>'Ya']);
            }else{
                DataTraining::where('nik',$k->nik)->update(['lanjut_usia'=>'Tidak']);

            }
        }
        DB::statement("SET SQL_MODE=''");
        return view('data-training.data-training',
        [
            'title'=>'Data Training',
            'training'=>DataTraining::all(),
            'datakk'=>KK::select('kode_keluarga','nama_anggota_keluarga','nik')->get(),
        ]);
    }

    public function edit($id)
    {
        $training = DataTraining::find($id);
        return response()->json([
            'status'=>200,
            'training'=> $training,
        ]);
    }

    public function destroy(Request $request)
    {
        DataTraining::where('id',$request->id)->delete();
        return back()->with('success','Data Berhasil Dihapus');
    }
}
