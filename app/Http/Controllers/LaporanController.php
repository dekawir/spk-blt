<?php

namespace App\Http\Controllers;

use App\Models\UjiDataBaru;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function blt()
    {
        if(empty(session('laporanblt'))){
            return redirect('uji-data-baru')->with('info','Uji Data Baru belum dihitung !!!');
        }
        return view('laporan.laporan-data-blt',[
            'title'=>'Laporan Data BLT',
            'klas'=>session('laporanblt'),
        ]);
    }

    public function spk()
    {
        if(empty(session('laporanspk'))){
            return redirect('uji-akurasi')->with('info','Uji Akurasi Baru belum dihitung !!!');
        }

        return view('laporan.laporan-data-spk',[
            'title'=>'Laporan Data SPK',
            'hasil'=>session('laporanspk'),
        ]);
    }
}
