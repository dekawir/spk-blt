<?php

namespace App\Http\Controllers;

use App\Models\DataTraining;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UjiAkurasiController extends Controller
{
    public function index(Request $request)
    {
        if($request->count){
            if(DataTraining::count()<1){
                return back()->with('info','Data Training Kosong!!!');
            }
            $validator = Validator::make($request->all(),[
                'persentase'=>'required|integer|between:20,60',
                'data_testing'=>'required',
            ]);

            if($validator->fails()){
                return back()->withErrors($validator);
            }

            $countTotal = DataTraining::count();
            $count = floor($countTotal * ($request->persentase/100));
            $sisaCount = $countTotal - $count;

            if($request->data_testing == "Acak"){
                $data_testing = "rand()";
            }else{
                $data_testing = "kode_keluarga";
            }
           
            $classPro = DB::select('SELECT
            (SELECT COUNT(blt) as tidak FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya") as ya, 
            (SELECT COUNT(blt) as tidak FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak") as tidak');
           
            $classProbabilitas = [
                'total'=> $sisaCount,
                'ya'=> $classPro[0]->ya,
                'tidak'=> $classPro[0]->tidak,
                'ya_total'=> $classPro[0]->ya/$sisaCount,
                'tidak_total'=> $classPro[0]->tidak/$sisaCount,
            ];

            $miskin_ekstrim = DB::select('SELECT
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya" AND miskin_ekstrim="Ya") as m_y, 
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya" AND miskin_ekstrim="Tidak") as m_t,
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak" AND miskin_ekstrim="Ya") as tm_y,
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak" AND miskin_ekstrim="Tidak") as tm_t
            ');

            $mata_pencaharian = DB::select('SELECT
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya" AND mata_pencaharian="Ya") as m_y, 
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya" AND mata_pencaharian="Tidak") as m_t,
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak" AND mata_pencaharian="Ya") as tm_y,
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak" AND mata_pencaharian="Tidak") as tm_t
            ');

            $sakit_menahun = DB::select('SELECT
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya" AND sakit_menahun="Ya") as m_y, 
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya" AND sakit_menahun="Tidak") as m_t,
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak" AND sakit_menahun="Ya") as tm_y,
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak" AND sakit_menahun="Tidak") as tm_t
            ');

            $keluarga_miskin = DB::select('SELECT
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya" AND keluarga_miskin="Ya") as m_y, 
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya" AND keluarga_miskin="Tidak") as m_t,
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak" AND keluarga_miskin="Ya") as tm_y,
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak" AND keluarga_miskin="Tidak") as tm_t
            ');

            $covid = DB::select('SELECT
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya" AND covid="Ya") as m_y, 
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya" AND covid="Tidak") as m_t,
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak" AND covid="Ya") as tm_y,
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak" AND covid="Tidak") as tm_t
            ');

            $lanjut_usia = DB::select('SELECT
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya" AND lanjut_usia="Ya") as m_y, 
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Ya" AND lanjut_usia="Tidak") as m_t,
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak" AND lanjut_usia="Ya") as tm_y,
            (SELECT COUNT(blt) as blt FROM (SELECT * FROM datatraining ORDER BY '.$data_testing.' LIMIT '.$sisaCount.' ) sq WHERE blt="Tidak" AND lanjut_usia="Tidak") as tm_t
            ');

            // dd($miskin_ekstrim[0]->m_y);
            $conditionalProbabilitas = [
                'miskin_ekstrim'=>[
                    'm_y'=> $miskin_ekstrim[0]->m_y,
                    'm_t'=> $miskin_ekstrim[0]->m_t,
                    'tm_y'=> $miskin_ekstrim[0]->tm_y,
                    'tm_t'=> $miskin_ekstrim[0]->tm_t,
                    'p_m_y'=> $miskin_ekstrim[0]->m_y/($miskin_ekstrim[0]->m_y+$miskin_ekstrim[0]->m_t),
                    'p_m_t'=> $miskin_ekstrim[0]->m_t/($miskin_ekstrim[0]->m_y+$miskin_ekstrim[0]->m_t),
                    'p_tm_y'=> $miskin_ekstrim[0]->tm_y/($miskin_ekstrim[0]->tm_y+$miskin_ekstrim[0]->tm_t),
                    'p_tm_t'=> $miskin_ekstrim[0]->tm_t/($miskin_ekstrim[0]->tm_y+$miskin_ekstrim[0]->tm_t),
                    'j_m'=> $miskin_ekstrim[0]->m_y+$miskin_ekstrim[0]->m_t,
                    'j_tm'=> $miskin_ekstrim[0]->tm_y+$miskin_ekstrim[0]->tm_t,
                    'j_p_m'=> ($miskin_ekstrim[0]->m_y/($miskin_ekstrim[0]->m_y+$miskin_ekstrim[0]->m_t))+($miskin_ekstrim[0]->m_t/($miskin_ekstrim[0]->m_y+$miskin_ekstrim[0]->m_t)),
                    'j_p_tm'=>($miskin_ekstrim[0]->tm_y/($miskin_ekstrim[0]->tm_y+$miskin_ekstrim[0]->tm_t))+($miskin_ekstrim[0]->tm_t/($miskin_ekstrim[0]->tm_y+$miskin_ekstrim[0]->tm_t))
                ],
                'mata_pencaharian'=>[
                    'm_y'=> $mata_pencaharian[0]->m_y,
                    'm_t'=> $mata_pencaharian[0]->m_t,
                    'tm_y'=> $mata_pencaharian[0]->tm_y,
                    'tm_t'=> $mata_pencaharian[0]->tm_t,
                    'p_m_y'=> $mata_pencaharian[0]->m_y/($mata_pencaharian[0]->m_y+$mata_pencaharian[0]->m_t),
                    'p_m_t'=> $mata_pencaharian[0]->m_t/($mata_pencaharian[0]->m_y+$mata_pencaharian[0]->m_t),
                    'p_tm_y'=> $mata_pencaharian[0]->tm_y/($mata_pencaharian[0]->tm_y+$mata_pencaharian[0]->tm_t),
                    'p_tm_t'=> $mata_pencaharian[0]->tm_t/($mata_pencaharian[0]->tm_y+$mata_pencaharian[0]->tm_t),
                    'j_m'=> $mata_pencaharian[0]->m_y+$mata_pencaharian[0]->m_t,
                    'j_tm'=> $mata_pencaharian[0]->tm_y+$mata_pencaharian[0]->tm_t,
                    'j_p_m'=> ($mata_pencaharian[0]->m_y/($mata_pencaharian[0]->m_y+$mata_pencaharian[0]->m_t))+($mata_pencaharian[0]->m_t/($mata_pencaharian[0]->m_y+$mata_pencaharian[0]->m_t)),
                    'j_p_tm'=>($mata_pencaharian[0]->tm_y/($mata_pencaharian[0]->tm_y+$mata_pencaharian[0]->tm_t))+($mata_pencaharian[0]->tm_t/($mata_pencaharian[0]->tm_y+$mata_pencaharian[0]->tm_t))
                ],
                'sakit_menahun'=>[
                    'm_y'=> $sakit_menahun[0]->m_y,
                    'm_t'=> $sakit_menahun[0]->m_t,
                    'tm_y'=> $sakit_menahun[0]->tm_y,
                    'tm_t'=> $sakit_menahun[0]->tm_t,
                    'p_m_y'=> $sakit_menahun[0]->m_y/($sakit_menahun[0]->m_y+$sakit_menahun[0]->m_t),
                    'p_m_t'=> $sakit_menahun[0]->m_t/($sakit_menahun[0]->m_y+$sakit_menahun[0]->m_t),
                    'p_tm_y'=> $sakit_menahun[0]->tm_y/($sakit_menahun[0]->tm_y+$sakit_menahun[0]->tm_t),
                    'p_tm_t'=> $sakit_menahun[0]->tm_t/($sakit_menahun[0]->tm_y+$sakit_menahun[0]->tm_t),
                    'j_m'=> $sakit_menahun[0]->m_y+$sakit_menahun[0]->m_t,
                    'j_tm'=> $sakit_menahun[0]->tm_y+$sakit_menahun[0]->tm_t,
                    'j_p_m'=> ($sakit_menahun[0]->m_y/($sakit_menahun[0]->m_y+$sakit_menahun[0]->m_t))+($sakit_menahun[0]->m_t/($sakit_menahun[0]->m_y+$sakit_menahun[0]->m_t)),
                    'j_p_tm'=>($sakit_menahun[0]->tm_y/($sakit_menahun[0]->tm_y+$sakit_menahun[0]->tm_t))+($sakit_menahun[0]->tm_t/($sakit_menahun[0]->tm_y+$sakit_menahun[0]->tm_t))
                ],
                'keluarga_miskin'=>[
                    'm_y'=> $keluarga_miskin[0]->m_y,
                    'm_t'=> $keluarga_miskin[0]->m_t,
                    'tm_y'=> $keluarga_miskin[0]->tm_y,
                    'tm_t'=> $keluarga_miskin[0]->tm_t,
                    'p_m_y'=> $keluarga_miskin[0]->m_y/($keluarga_miskin[0]->m_y+$keluarga_miskin[0]->m_t),
                    'p_m_t'=> $keluarga_miskin[0]->m_t/($keluarga_miskin[0]->m_y+$keluarga_miskin[0]->m_t),
                    'p_tm_y'=> $keluarga_miskin[0]->tm_y/($keluarga_miskin[0]->tm_y+$keluarga_miskin[0]->tm_t),
                    'p_tm_t'=> $keluarga_miskin[0]->tm_t/($keluarga_miskin[0]->tm_y+$keluarga_miskin[0]->tm_t),
                    'j_m'=> $keluarga_miskin[0]->m_y+$keluarga_miskin[0]->m_t,
                    'j_tm'=> $keluarga_miskin[0]->tm_y+$keluarga_miskin[0]->tm_t,
                    'j_p_m'=> ($keluarga_miskin[0]->m_y/($keluarga_miskin[0]->m_y+$keluarga_miskin[0]->m_t))+($keluarga_miskin[0]->m_t/($keluarga_miskin[0]->m_y+$keluarga_miskin[0]->m_t)),
                    'j_p_tm'=>($keluarga_miskin[0]->tm_y/($keluarga_miskin[0]->tm_y+$keluarga_miskin[0]->tm_t))+($keluarga_miskin[0]->tm_t/($keluarga_miskin[0]->tm_y+$keluarga_miskin[0]->tm_t))
                ],
                'covid'=>[
                    'm_y'=> $covid[0]->m_y,
                    'm_t'=> $covid[0]->m_t,
                    'tm_y'=> $covid[0]->tm_y,
                    'tm_t'=> $covid[0]->tm_t,
                    'p_m_y'=> $covid[0]->m_y/($covid[0]->m_y+$covid[0]->m_t),
                    'p_m_t'=> $covid[0]->m_t/($covid[0]->m_y+$covid[0]->m_t),
                    'p_tm_y'=> $covid[0]->tm_y/($covid[0]->tm_y+$covid[0]->tm_t),
                    'p_tm_t'=> $covid[0]->tm_t/($covid[0]->tm_y+$covid[0]->tm_t),
                    'j_m'=> $covid[0]->m_y+$covid[0]->m_t,
                    'j_tm'=> $covid[0]->tm_y+$covid[0]->tm_t,
                    'j_p_m'=> ($covid[0]->m_y/($covid[0]->m_y+$covid[0]->m_t))+($covid[0]->m_t/($covid[0]->m_y+$covid[0]->m_t)),
                    'j_p_tm'=>($covid[0]->tm_y/($covid[0]->tm_y+$covid[0]->tm_t))+($covid[0]->tm_t/($covid[0]->tm_y+$covid[0]->tm_t))
                ],
                'lanjut_usia'=>[
                    'm_y'=> $lanjut_usia[0]->m_y,
                    'm_t'=> $lanjut_usia[0]->m_t,
                    'tm_y'=> $lanjut_usia[0]->tm_y,
                    'tm_t'=> $lanjut_usia[0]->tm_t,
                    'p_m_y'=> $lanjut_usia[0]->m_y/($lanjut_usia[0]->m_y+$lanjut_usia[0]->m_t),
                    'p_m_t'=> $lanjut_usia[0]->m_t/($lanjut_usia[0]->m_y+$lanjut_usia[0]->m_t),
                    'p_tm_y'=> $lanjut_usia[0]->tm_y/($lanjut_usia[0]->tm_y+$lanjut_usia[0]->tm_t),
                    'p_tm_t'=> $lanjut_usia[0]->tm_t/($lanjut_usia[0]->tm_y+$lanjut_usia[0]->tm_t),
                    'j_m'=> $lanjut_usia[0]->m_y+$lanjut_usia[0]->m_t,
                    'j_tm'=> $lanjut_usia[0]->tm_y+$lanjut_usia[0]->tm_t,
                    'j_p_m'=> ($lanjut_usia[0]->m_y/($lanjut_usia[0]->m_y+$lanjut_usia[0]->m_t))+($lanjut_usia[0]->m_t/($lanjut_usia[0]->m_y+$lanjut_usia[0]->m_t)),
                    'j_p_tm'=>($lanjut_usia[0]->tm_y/($lanjut_usia[0]->tm_y+$lanjut_usia[0]->tm_t))+($lanjut_usia[0]->tm_t/($lanjut_usia[0]->tm_y+$lanjut_usia[0]->tm_t))
                ],
            ];

            $datakk = DB::select('SELECT * FROM datatraining ORDER BY kode_keluarga DESC LIMIT '.$count.' ');
            foreach ($datakk as $no=>$k){
            
                if($k->miskin_ekstrim =='Ya'){
                    $a_c1=$conditionalProbabilitas['miskin_ekstrim']['p_m_y'];
                    $a_c0=$conditionalProbabilitas['miskin_ekstrim']['p_tm_y'];
                }else{
                    $a_c1=$conditionalProbabilitas['miskin_ekstrim']['p_m_t'];
                    $a_c0=$conditionalProbabilitas['miskin_ekstrim']['p_tm_t'];
                }
                
                if($k->mata_pencaharian =='Ya'){
                    $b_c1=$conditionalProbabilitas['mata_pencaharian']['p_m_y'];
                    $b_c0=$conditionalProbabilitas['mata_pencaharian']['p_tm_y'];
                }else{
                    $b_c1=$conditionalProbabilitas['mata_pencaharian']['p_m_t'];
                    $b_c0=$conditionalProbabilitas['mata_pencaharian']['p_tm_t'];
                }

                if($k->sakit_menahun =='Ya'){
                    $c_c1=$conditionalProbabilitas['sakit_menahun']['p_m_y'];
                    $c_c0=$conditionalProbabilitas['sakit_menahun']['p_tm_y'];
                }else{
                    $c_c1=$conditionalProbabilitas['sakit_menahun']['p_m_t'];
                    $c_c0=$conditionalProbabilitas['sakit_menahun']['p_tm_t'];
                }

                if($k->keluarga_miskin =='Ya'){
                    $d_c1=$conditionalProbabilitas['keluarga_miskin']['p_m_y'];
                    $d_c0=$conditionalProbabilitas['keluarga_miskin']['p_tm_y'];
                }else{
                    $d_c1=$conditionalProbabilitas['keluarga_miskin']['p_m_t'];
                    $d_c0=$conditionalProbabilitas['keluarga_miskin']['p_tm_t'];
                }

                if($k->covid =='Ya'){
                    $e_c1=$conditionalProbabilitas['covid']['p_m_y'];
                    $e_c0=$conditionalProbabilitas['covid']['p_tm_y'];
                }else{
                    $e_c1=$conditionalProbabilitas['covid']['p_m_t'];
                    $e_c0=$conditionalProbabilitas['covid']['p_tm_t'];
                }

                if($k->lanjut_usia =='Ya'){
                    $f_c1=$conditionalProbabilitas['lanjut_usia']['p_m_y'];
                    $f_c0=$conditionalProbabilitas['lanjut_usia']['p_tm_y'];
                }else{
                    $f_c1=$conditionalProbabilitas['lanjut_usia']['p_m_t'];
                    $f_c0=$conditionalProbabilitas['lanjut_usia']['p_tm_t'];
                }

                $c1 = $classProbabilitas['ya_total']*$a_c1*$b_c1*$c_c1*$d_c1*$e_c1*$f_c1;
                $c0 = $classProbabilitas['tidak_total']*$a_c0*$b_c0*$c_c0*$d_c0*$e_c0*$f_c0;
                
                if($c1>$c0){
                    $blt = 'Menerima';
                }else{
                    $blt = 'Tidak Menerima';
                }
                $perhitungan[] = [
                    'nama_anggota_keluarga'=>$k->nama_anggota_keluarga,
                    'nik'=>$k->nik,
                    'c1'=>$c1,
                    'c0'=>$c0,
                    'blt'=> $blt
                    ];
            }

            foreach($perhitungan as $nop=>$p){
                if($datakk[$nop]->blt=='Ya'){
                    $act='Menerima';
                }else{
                    $act='Tidak Menerima';

                }

                $dataConf[] = [
                    'nama_anggota_keluarga'=>$p['nama_anggota_keluarga'],
                    'nik'=>$p['nik'],
                    'blt'=> $p['blt'],
                    'bltact'=>$act
                ];

            }
            $tp=0;
            $fp=0;
            $fn=0;
            $tn=0;
            foreach($dataConf as $noc=>$t){
                if($t['blt']=='Menerima' && $t['bltact']=='Menerima'){
                    $tp +=1;
                }
                if($t['blt']=='Menerima' && $t['bltact']=='Tidak Menerima'){
                    $fp +=1;
                }
                if($t['blt']=='Tidak Menerima' && $t['bltact']=='Menerima'){
                    $fn +=1;
                }
                if($t['blt']=='Tidak Menerima' && $t['bltact']=='Tidak Menerima'){
                    $tn +=1;
                }

                $conf = [
                    'tp'=>$tp,
                    'fp'=>$fp,
                    'fn'=>$fn,
                    'tn'=>$tn,
                ];
            }

            $hasil = [
                'persentase'=>$request->persentase,
                'data'=>$request->data_testing,
                'acc'=> ($conf['tp']+$conf['tn'])/($conf['tp']+$conf['tn']+($conf['fp']+$conf['fn'])),
                'pre'=> $conf['tp']/($conf['tp']+$conf['fp']),
                'rec'=> $conf['tp']/($conf['tp']+$conf['fn']),
            ];

            // dd($hasil);
            session()->put([
                // 'laporanblt'=>$dataConf,
                'laporanspk'=>$hasil,
            ]);


            return view('uji-akurasi.uji-akurasi',[
                'title'=>'Uji Akurasi',
                'classPro'=> $classProbabilitas,
                'condPro'=> $conditionalProbabilitas,
                'persentase'=> $request->persentase,
                'data_testing'=> $request->data_testing,
                'testing'=> $datakk,
                'normalisasi'=> '',
                'perhitungan'=> $perhitungan,
                'klas'=>$dataConf,
                'conf'=>$conf,
                'hasil'=>$hasil,
            ]);
        }
        return view('uji-akurasi.uji-akurasi',[
            'title'=>'Uji Akurasi',
            'classPro'=> '',
            'condPro'=> '',
            'persentase'=> '',
            'data_testing'=> '',
        ]);
    }
}
