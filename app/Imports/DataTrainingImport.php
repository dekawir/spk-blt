<?php

namespace App\Imports;

use App\Models\DataTraining;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;

class DataTrainingImport implements ToModel, WithHeadingRow, WithValidation
{
   
    use Importable;
    public function rules(): array
        {
            return [
                'kode_keluarga'=>'required|unique:App\Models\DataTraining,kode_keluarga',
                'nama_anggota_keluarga'=>'required',
                'nik'=>'required',
                'miskin_ekstrim'=>'required',
                'mata_pencaharian'=>'required',
                'sakit_menahun'=>'required',
                'keluarga_miskin'=>'required',
                'covid'=>'required',
                'lanjut_usia'=>'required',
                'blt'=>'required',
            ];

        }

        public function customValidationMessages()
        {
            return [
                'kode_keluarga'=> 'Kolom Kode Keluarga masih kosong!!!',
                'nama_anggota_keluarga'=> 'Kolom Nama Anggota Keluarga Kelamin masih kosong!!!',
                'nik'=> 'Kolom NIK Kelamin masih kosong!!!',
                'miskin_ekstrim'=> 'Kolom Miskin Ekstrim Kepala Keluarga masih kosong!!!',
                'mata_pencaharian'=> 'Kolom Mata Pecaharian masih kosong!!!',
                'sakit_menahun'=> 'Kolom Sakit Menahun masih kosong!!!' ,
                'keluarga_miskin'=> 'Kolom Keluarga Miskin masih kosong!!!',
                'covid'=> 'Kolom Covid masih kosong!!!',
                'hubungan'=> 'Kolom Hubungan masih kosong!!!',
                'lanjut_usia'=> 'Kolom Lanjut Usia masih kosong!!!',
                'blt'=> 'Kolom BLT masih kosong!!!',
                
            ];
        }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }

    public function model(array $row)
    {
        // dd($row);
        return new DataTraining([
            'kode_keluarga'=>$row['kode_keluarga'],
            'nama_anggota_keluarga'=>$row['nama_anggota_keluarga'],
            'nik'=>$row['nik'],
            'miskin_ekstrim'=>$row['miskin_ekstrim'],
            'mata_pencaharian'=>$row['mata_pencaharian'],
            'sakit_menahun'=>$row['sakit_menahun'],
            'keluarga_miskin'=>$row['keluarga_miskin'],
            'covid'=>$row['covid'],
            'lanjut_usia'=>$row['lanjut_usia'],
            'blt'=>$row['blt'],
        ]);
    }
}
