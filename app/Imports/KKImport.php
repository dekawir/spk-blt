<?php

namespace App\Imports;

use App\Models\KK;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;

class KKImport implements ToModel, WithHeadingRow, WithValidation
{
   
    use Importable;
    public function rules(): array
        {
            return [
                'alamat' => 'required',
                'kode_keluarga'=> 'required|numeric',
                'nama_kepala_keluarga'=> 'required',
                'no'=> 'required|numeric',
                'nik'=> 'required|numeric' ,
                'nama_anggota_keluarga'=> 'required',
                'jenis_kelamin'=> 'required',
                'hubungan'=> 'required',
                'tempat_lahir'=> 'required',
                'tanggal_lahir'=> 'required',
                'usia'=> 'required',
                'status'=> 'required',
                'agama'=> 'required',
                'golongan_darah'=> 'required',
                'kewarganegaraan'=> 'required',
                'pendidikan'=> 'required',
                'pekerjaan'=> 'required',
            ];

        }

        public function customValidationMessages()
        {
            return [
                'alamat' => 'Kolom Alamat masih kosong!!!',
                'kode_keluarga'=> 'Kolom Kode Keluarga Kelamin masih kosong!!!',
                'nama_kepala_keluarga'=> 'Kolom Nama Kepala Keluarga masih kosong!!!',
                'no'=> 'Kolom No Orang Tua masih kosong!!!',
                'nik'=> 'Kolom NIK masih kosong!!!' ,
                'nama_anggota_keluarga'=> 'Kolom Nama Anggota Keluarga masih kosong!!!',
                'jenis_kelamiin'=> 'Kolom Jenis Kelamin masih kosong!!!',
                'hubungan'=> 'Kolom Hubungan masih kosong!!!',
                'tempat_lahir'=> 'Kolom Tempat Lahir masih kosong!!!',
                'tanggal_lahir'=> 'Kolom Tanggal Lahir masih kosong!!!',
                'usia'=> 'Kolom Usia masih kosong!!!',
                'status'=> 'Kolom Status masih kosong!!!',
                'agama'=> 'Kolom Agama masih kosong!!!',
                'golongan_darah'=> 'Kolom Golongan Darah masih kosong!!!',
                'kewarganegaraan'=> 'Kolom Kewarganegaraan masih kosong!!!',
                'pendidikan'=> 'Kolom Pendidikan masih kosong!!!',
                'pekerjaan'=> 'Kolom Pekerjaan masih kosong!!!',
            ];
        }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }

    public function model(array $row)
    {
        // dd($row);
        return new KK([
            'alamat'=> $row['alamat'],
            'kode_keluarga'=> $row['kode_keluarga'],
            'nama_kepala_keluarga'=> $row['nama_kepala_keluarga'],
            'no'=> $row['no'],
            'nik'=> $row['nik'],
            'nama_anggota_keluarga'=> $row['nama_anggota_keluarga'],
            'jenis_kelamin'=> $row['jenis_kelamin'],
            'hubungan'=> $row['hubungan'],
            'tempat_lahir'=> $row['tempat_lahir'],
            'tanggal_lahir'=> $this->transformDate($row['tanggal_lahir']) ,
            'usia'=> $row['usia'],
            'status'=> $row['status'],
            'agama'=> $row['agama'],
            'golongan_darah'=> $row['golongan_darah'],
            'kewarganegaraan'=> $row['kewarganegaraan'],
            'pendidikan'=> $row['pendidikan'],
            'pekerjaan'=> $row['pekerjaan'],
        ]);
    }
}
