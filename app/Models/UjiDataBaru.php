<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UjiDataBaru extends Model
{
    use HasFactory;

    protected $table ='ujidatabaru';
    protected $guarded = ['id'];
}
