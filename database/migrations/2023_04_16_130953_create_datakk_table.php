<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datakk', function (Blueprint $table) {
            $table->id();
            $table->string('alamat',50);
            $table->string('kode_keluarga',16);
            $table->string('nama_kepala_keluarga',100);
            $table->string('no',5);
            $table->string('nik',16);
            $table->string('nama_anggota_keluarga',100);
            $table->string('jenis_kelamin',20);
            $table->string('hubungan',50);
            $table->string('tempat_lahir',100);
            $table->string('tanggal_lahir',20);
            $table->string('usia',5);
            $table->string('status',50);
            $table->string('agama',50);
            $table->string('golongan_darah',50);
            $table->string('kewarganegaraan',50);
            $table->string('pendidikan',50);
            $table->string('pekerjaan',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datakk');
    }
};
