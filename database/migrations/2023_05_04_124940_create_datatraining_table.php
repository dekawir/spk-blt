<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datatraining', function (Blueprint $table) {
            $table->id();
            $table->string('kode_keluarga',16);
            $table->string('nama_anggota_keluarga',50);
            $table->string('nik',16);
            $table->string('miskin_ekstrim',50);
            $table->string('mata_pencaharian',50);
            $table->string('sakit_menahun',50);
            $table->string('keluarga_miskin',50);
            $table->string('covid',50);
            $table->string('lanjut_usia',50);
            $table->string('blt',50);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datatraining');
    }
};
