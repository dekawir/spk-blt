<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name'=>'Kepala Desa',
                'username' => 'kades',
                'email'=>'kades@example.com',
                'level'=>'kades',
                'password'=> bcrypt('123456'),
            ],
            [
                'name'=>'Staff Desa',
                'username' => 'staff',
                'email'=>'staff@example.com',
                'level'=>'staff',
                'password'=> bcrypt('123456'),
            ],
        ];

        foreach ($user as $key => $value) {
            User::create($value);
            
        }
    }
}
