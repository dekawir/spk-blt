@extends('layouts.index')
@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap5.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
@endpush
@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">{{ $title }} Desa Buahan Kaja</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2">
                            {{-- <button id="modalForm"  class="btn btn-primary">Tambah <i class="fas fa-plus"></i></button> --}}
                            <a class="modal-with-form btn btn-primary" href="#modalForm">Tambah <i class="fas fa-plus"></i></a>
                            
                            <!-- Modal Form -->
                            <div id="modalForm" class="modal-block modal-block-primary mfp-hide">
                                <section class="card">
                                    <header class="card-header">
                                        <h2 class="card-title">Form Tambah {{ $title }}</h2>
                                    </header>
                                    <form method="POST" action="">
                                        @csrf
                                        <div class="card-body">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="">No. KK :</label>
                                                    <input onkeypress="return hanyaAngka(event)" data-plugin-maxlength maxlength="16" type="text" class="form-control" value="{{ old('kode_keluarga') }}"  placeholder="No. KK" name="kode_keluarga">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="">Nama Kepala Keluarga :</label>
                                                    <input type="text" class="form-control" id="nama_kepala_keluarga" value="{{ old('nama_kepala_keluarga') }}" placeholder="Nama Kepala Keluarga" name="nama_kepala_keluarga">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="">No :</label>
                                                    <input onkeypress="return hanyaAngka(event)" type="text" class="form-control" id="no" value="{{ old('no') }}"  placeholder="No" name="no">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="">NIK :</label>
                                                    <input onkeypress="return hanyaAngka(event)"  data-plugin-maxlength maxlength="16" type="text" class="form-control" id="nik" value="{{ old('nik') }}"  placeholder="NIK" name="nik">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <label>Jenis Kelamin :</label>
                                                <div class="col-lg-12">
                                                    <select data-plugin-selectTwo class="form-control populate" id="jenis_kelamin" name="jenis_kelamin">
                                                        <option value="">Pilih Jenis Kelamin</option>
                                                        <option {{ old('jenis_kelamin')== 'LAKI-LAKI' ? 'selected':''}} value="LAKI-LAKI">LAKI-LAKI</option>
                                                        <option {{ old('jenis_kelamin')== 'PEREMPUAN' ? 'selected':''}} value="PEREMPUAN">PEREMPUAN</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <label>Hubungan :</label>
                                                <div class="col-lg-12">
                                                    <select data-plugin-selectTwo class="form-control populate" id="hubungan" name="hubungan" >
                                                        <option {{ old('hubungan')== 'Kepala Keluarga' ? 'selected':''}} selected value="Kepala Keluarga">Kepala Keluarga</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="">Tempat Lahir :</label>
                                                    <input type="text" class="form-control" id="tempat_lahir"  placeholder="Tempat Lahir" name="tempat_lahir" value="{{ old('tempat_lahir') }}">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <label>Tanggal Lahir</label>
                                                <div class="col-lg-12">
                                                    <div class="input-group">
                                                        <span class="input-group-text">
                                                            <i class="fas fa-calendar-alt"></i>
                                                        </span>
                                                        <input type="text" data-plugin-datepicker id="tanggal_lahir" class="form-control" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="">Usia :</label>
                                                    <input onkeypress="return hanyaAngka(event)" type="text" class="form-control" id="usia"  placeholder="Usia" name="usia" value="{{ old('usia') }}">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <label>Status :</label>
                                                <div class="col-lg-12">
                                                    <select data-plugin-selectTwo class="form-control populate" id="status" name="status" >
                                                        <option value="">Pilih Status</option>
                                                        <option {{ old('status')== 'Belum Kawin' ? 'selected':''}} value="Belum Kawin">Belum Kawin</option>
                                                        <option {{ old('status')== 'Janda/Duda' ? 'selected':''}} value="Janda/Duda">Janda/Duda</option>
                                                        <option {{ old('status')== 'Kawin' ? 'selected':''}} value="Kawin">Kawin</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <label>Agama :</label>
                                                <div class="col-lg-12">
                                                    <select data-plugin-selectTwo class="form-control populate" id="agama" name="agama" >
                                                        <option value="">Pilih Agama</option>
                                                        <option {{ old('agama')== 'Hindu' ? 'selected':''}} value="Hindu">Hindu</option>
                                                        <option {{ old('agama')== 'Islam' ? 'selected':''}} value="Islam">Islam</option>
                                                        <option {{ old('agama')== 'Katholik' ? 'selected':''}} value="Katholik">Katholik</option>
                                                        <option {{ old('agama')== 'Buda' ? 'selected':''}} value="Buda">Buda</option>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="form-row">
                                                <label>Pendidikan :</label>
                                                <div class="col-lg-12">
                                                    <select data-plugin-selectTwo class="form-control populate" id="pendidikan" name="pendidikan">
                                                        <option value="">Pilih Pendidikan</option>
                                                        <option {{ old('pendidikan')== 'Belum masuk TK/Kelompok Bermain' ? 'selected':''}} value="Belum masuk TK/Kelompok Bermain">Belum masuk TK/Kelompok Bermain</option>
                                                        <option {{ old('pendidikan')== 'Sedang D-1/sederajat' ? 'selected':''}} value="Sedang D-1/sederajat">Sedang D-1/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Sedang D-3/sederajat' ? 'selected':''}} value="Sedang D-3/sederajat">Sedang D-3/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Sedang S-1/sederajat' ? 'selected':''}} value="Sedang S-1/sederajat">Sedang S-1/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Sedang SLTA/sederajat' ? 'selected':''}} value="Sedang SLTA/sederajat">Sedang SLTA/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Sedang SLTP/Sederajat' ? 'selected':''}} value="Sedang SLTP/Sederajat">Sedang SLTP/Sederajat</option>
                                                        <option {{ old('pendidikan')== 'Sedang TK/Kelompok Bermain' ? 'selected':''}} value="Sedang TK/Kelompok Bermain">Sedang TK/Kelompok Bermain</option>
                                                        <option {{ old('pendidikan')== 'Tamat D-1/sederajat' ? 'selected':''}} value="Tamat D-1/sederajat">Tamat D-1/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Tamat D-2/sederajat' ? 'selected':''}} value="Tamat D-2/sederajat">Tamat D-2/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Tamat D-3/sederajat' ? 'selected':''}} value="Tamat D-3/sederajat">Tamat D-3/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Tamat D-4/sederajat' ? 'selected':''}} value="Tamat D-4/sederajat">Tamat D-4/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Tamat S-1/sederajat' ? 'selected':''}} value="Tamat S-1/sederajat">Tamat S-1/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Tamat S-2/sederajat' ? 'selected':''}} value="Tamat S-2/sederajat">Tamat S-2/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Tamat SD/sederajat' ? 'selected':''}} value="Tamat SD/sederajat">Tamat SD/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Tamat SLTA/sederajat' ? 'selected':''}} value="Tamat SLTA/sederajat">Tamat SLTA/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Tamat SLTP/sederajat' ? 'selected':''}} value="Tamat SLTP/sederajat">Tamat SLTP/sederajat</option>
                                                        <option {{ old('pendidikan')== 'Tidak pernah sekolah' ? 'selected':''}} value="Tidak pernah sekolah">Tidak pernah sekolah</option>
                                                        <option {{ old('pendidikan')== 'Tidak tamat SD/sederajat' ? 'selected':''}} value="Tidak tamat SD/sederajat">Tidak tamat SD/sederajat</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <label>Pekerjaan :</label>
                                                <div class="col-lg-12">
                                                    <select data-plugin-selectTwo class="form-control populate" id="pekerjaan" name="pekerjaan">
                                                        <option value="">Pilih Pekerjaan</option>
                                                        <option {{ old('pekerjaan')== 'Belum Bekerja' ? 'selected':''}} value="Belum Bekerja">Belum Bekerja</option>
                                                        <option {{ old('pekerjaan')== 'Bidan swasta' ? 'selected':''}} value="Bidan swasta">Bidan swasta</option>
                                                        <option {{ old('pekerjaan')== 'Buruh Harian Lepas' ? 'selected':''}} value="Buruh Harian Lepas">Buruh Harian Lepas</option>
                                                        <option {{ old('pekerjaan')== 'Buruh Tani' ? 'selected':''}} value="Buruh Tani">Buruh Tani</option>
                                                        <option {{ old('pekerjaan')== 'Dosen swasta' ? 'selected':''}} value="Dosen swasta">Dosen swasta</option>
                                                        <option {{ old('pekerjaan')== 'Ibu Rumah Tangga' ? 'selected':''}} value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                                                        <option {{ old('pekerjaan')== 'Juru Masak' ? 'selected':''}} value="Juru Masak">Juru Masak</option>
                                                        <option {{ old('pekerjaan')== 'Karyawan Honorer' ? 'selected':''}} value="Karyawan Honorer">Karyawan Honorer</option>
                                                        <option {{ old('pekerjaan')== 'Karyawan Perusahaan Pemerintah' ? 'selected':''}} value="Karyawan Perusahaan Pemerintah">Karyawan Perusahaan Pemerintah</option>
                                                        <option {{ old('pekerjaan')== 'Karyawan Perusahaan Swasta' ? 'selected':''}} value="Karyawan Perusahaan Swasta">Karyawan Perusahaan Swasta</option>
                                                        <option {{ old('pekerjaan')== 'Montir' ? 'selected':''}} value="Montir">Montir</option>
                                                        <option {{ old('pekerjaan')== 'Pedagang barang kelontong' ? 'selected':''}} value="Pedagang barang kelontong">Pedagang barang kelontong</option>
                                                        <option {{ old('pekerjaan')== 'Pegawai Negeri Sipil' ? 'selected':''}} value="Pegawai Negeri Sipil">Pegawai Negeri Sipil</option>
                                                        <option {{ old('pekerjaan')== 'Pelajar' ? 'selected':''}} value="Pelajar">Pelajar</option>
                                                        <option {{ old('pekerjaan')== 'Pengusaha kecil, menengah dan besar' ? 'selected':''}} value="Pengusaha kecil, menengah dan besar">Pengusaha kecil, menengah dan besar</option>
                                                        <option {{ old('pekerjaan')== 'Perangkat Desa' ? 'selected':''}} value="Perangkat Desa">Perangkat Desa</option>
                                                        <option {{ old('pekerjaan')== 'Perawat swasta' ? 'selected':''}} value="Perawat swasta">Perawat swasta</option>
                                                        <option {{ old('pekerjaan')== 'Petani' ? 'selected':''}} value="Petani">Petani</option>
                                                        <option {{ old('pekerjaan')== 'Peternak' ? 'selected':''}} value="Peternak">Peternak</option>
                                                        <option {{ old('pekerjaan')== 'POLRI' ? 'selected':''}} value="POLRI">POLRI</option>
                                                        <option {{ old('pekerjaan')== 'Purnawirawan/Pensiunan' ? 'selected':''}} value="Purnawirawan/Pensiunan">Purnawirawan/Pensiunan</option>
                                                        <option {{ old('pekerjaan')== 'Sopir' ? 'selected':''}} value="Sopir">Sopir</option>
                                                        <option {{ old('pekerjaan')== 'Tidak Mempunyai Pekerjaan Tetap' ? 'selected':''}} value="Tidak Mempunyai Pekerjaan Tetap">Tidak Mempunyai Pekerjaan Tetap</option>
                                                        <option {{ old('pekerjaan')== 'TNI' ? 'selected':''}} value="TNI">TNI</option>
                                                        <option {{ old('pekerjaan')== 'Tukang Kayu' ? 'selected':''}} value="Tukang Kayu">Tukang Kayu</option>
                                                        <option {{ old('pekerjaan')== 'Tukang Rias' ? 'selected':''}} value="Tukang Rias">Tukang Rias</option>
                                                        <option {{ old('pekerjaan')== 'Wiraswasta' ? 'selected':''}} value="Wiraswasta">Wiraswasta</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="">Alamat :</label>
                                                    <input type="text" class="form-control" id="alamat"  placeholder="Alamat" name="alamat" value="{{ old('alamat') }}">
                                                </div>
                                            </div>
                                            </div>
                                            <footer class="card-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-end">
                                                        <button type="submit" value="add" name="add" class="btn btn-primary">Submit</button>
                                                        <button class="btn btn-default modal-dismiss">Cancel</button>
                                                    </div>
                                                </div>
                                            </footer>
                                    </form>
                                </section>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            {{-- <a class="btn btn-danger" href="">Kosongkan <i class="fas fa-trash"></i></a> --}}
                            <a href="#modalAnimDel" class="modal-with-move-anim ws-normal btn btn-danger">Hapus <i class="fas fa-trash"></i></a>

                        </div>
                        <div class="col-sm-8">
                            <form action="" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fas fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Change</span>
                                            <span class="fileupload-new">Select file</span>
                                            <input type="file" name="file" />
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        <button type="submit" name="import" value="import" class="btn btn-warning">Import <i class="fas fa-database"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                    <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">

                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>No. KK</th>
                                <th>Nama Kepala Keluarga</th>
                                <th>NIK</th>
                                <th>Alamat</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no =1;
                            @endphp
                            @foreach ($kk as $u)
                            <tr>                                    
                                <td>{{ $no++ }}</td>
                                <td>{{ $u->kode_keluarga }}</td>
                                <td>{{ $u->nama_kepala_keluarga }}</td>
                                <td>{{ $u->nik }}</td>
                                <td>{{ $u->alamat }}</td>
                                <td>
                                    {{-- <a  onclick="editbtn({{ $u->id }})" href="#editForm" class="modal-with-form btn btn-primary btn btn-primary">Edit <i class="bx bx-edit"></i></a>| --}}
                                    <a  href="{{URL::to('data-kk-detail')}}/{{ $u->kode_keluarga }}" class="btn btn-primary">Detail <i class="bx bx-file-find"></i></a>|
                                    <a onclick="del({{ $u->kode_keluarga }})" href="#modalAnim" class="modal-with-move-anim ws-normal btn btn-warning">Hapus <i class="bx bx-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
<!-- end: page -->

<!-- Modal Animation -->
<div id="modalAnim" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Are you sure?</h2>
        </header>
        <form action="{{ route('del.datakk') }}" method="POST">
            @csrf
            <div class="card-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fas fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p class="mb-0">Apakah anda yakin menghapus data ini?</p>
                        <input type="hidden" id="delUser" name="id">
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-end">
                        <button type="submit" class="btn btn-primary">Confirm</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </form>
    </section>
</div>


{{-- hapus semua data --}}
{{-- kosongkan data --}}

<div id="modalAnimDel" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Konfirmasi</h2>
        </header>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fas fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p class="mb-0">Apakah anda yakin mengkosongkan semua data ini?</p>
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-end">
                        <button type="submit" name="truncate" value="truncate" class="btn btn-primary">Confirm</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </form>
    </section>
</div>



<script>
    function editbtn(id){
        var input = document.getElementById("editId");
        input.value = id;

        // console.log(id);
        // $('#editForm').modal('show');
        $.ajax({
            type: "GET",
            url: "/data-kk-edit/"+id,
            success: function (response) {
                console.log(response,id);

                $('#nama_kk').val(response.kk.nama_kk);
                $('#no_kk').val(response.kk.no_kk);
                $('#nik').val(response.kk.nik);
                $('#alamat').val(response.kk.alamat).change();
            }
        });
        // alert(id);
        // $('#modalUpdate').modal('show');
    }
</script>

{{-- Skrip tarik data modal confirmation --}}
<script>
    function del(id) {
        // $('#modal-category_name').html(id);
        var input = document.getElementById("delUser");
        input.value = id;
        // console.log(id);
        // $('#modal-confirm_delete').attr('onclick', 'confirmDelete(${id})');
        // $('#modalAnim').modal('show');
    }

    
</script>


@endsection



@push('js')
<!-- Specific Page Vendor -->
<script src="{{ asset('vendor/select2/js/select2.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js')}}"></script>


<script src="{{ asset('vendor/autosize/autosize.js')}}"></script>
<script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js')}}"></script>
@endpush

@push('example')

<!-- Examples -->
<script src="{{ asset('js/examples/examples.datatables.default.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.row.with.details.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.tabletools.js')}}"></script>

<!-- Examples -->
<script>
(function($) {

'use strict';



$('.modal-with-move-anim').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,
    
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom',
    modal: true
});

/*
Modal Dismiss
*/
$(document).on('click', '.modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
});

/*
Modal Confirm
*/
$(document).on('click', '.modal-confirm', function (e) {
    e.preventDefault();
    $.magnificPopup.close();

    const del = document.querySelector("#delete");

    // console.log(del.dataset);
    // console.log('haii');
});

/*
Form
*/
$('.modal-with-form').magnificPopup({
    type: 'inline',
    preloader: false,
    focus: '#name',
    modal: true,

    // When elemened is focused, some mobile browsers in some cases zoom in
    // It looks not nice, so we disable it:
    callbacks: {
        beforeOpen: function() {
            if($(window).width() < 700) {
                this.st.focus = false;
            } else {
                this.st.focus = '#name';
            }
        }
    }
});

/*
Ajax
*/
$('.simple-ajax-modal').magnificPopup({
    type: 'ajax',
    modal: true
});

}).apply(this, [jQuery]);
</script>
@endpush