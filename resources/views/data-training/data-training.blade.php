@extends('layouts.index')
@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap5.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />

    <style>
        tr,th{
            vertical-align: middle;
        }
    </style>
@endpush
@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">{{ $title }} Desa Buahan Kaja</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2">
                            {{-- <button id="modalForm"  class="btn btn-primary">Tambah <i class="fas fa-plus"></i></button> --}}
                            <a class="modal-with-form btn btn-primary" href="#modalForm">Tambah <i class="fas fa-plus"></i></a>
                            <!-- Modal Form -->
                            <div id="modalForm" class="modal-block modal-block-primary mfp-hide">
                                <section class="card">
                                    <header class="card-header">
                                        <h2 class="card-title">Form Tambah {{ $title }}</h2>
                                    </header>
                                    <form method="POST" action="">
                                        @csrf
                                        <div class="card-body"> 
                                            <div class="form-row">
                                                <label>No. KK :</label>
                                                <div class="col-lg-12">
                                                    <select data-plugin-selectTwo class="form-control populate selectVal" name="nik" required>
                                                        <option value="" >Pilih No. KK - NIK - Nama</option>
                                                        @foreach ($datakk as $i)
                                                        <option {{ old('nik')== $i->nik ? 'selected':''}} value="{{ $i->nik }}">{{ $i->kode_keluarga .' - '.$i->nik .' - '. $i->nama_anggota_keluarga}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <label class=""">Checkboxes :</label>
                                                <div class="col-lg-12">
                                                    <div class="checkbox-custom checkbox-default">
                                                        <input type="checkbox" id="checkboxExample1" value="Ya" name="miskin_ekstrim">
                                                        <label for="checkboxExample1">MISKIN EKSTRIM</label>
                                                    </div>

                                                    <div class="checkbox-custom checkbox-primary">
                                                        <input type="checkbox" id="checkboxExample2" value="Ya" name="mata_pencaharian">
                                                        <label for="checkboxExample2">KEHILANGAN MATA PENCAHARIAN</label>
                                                    </div>

                                                    <div class="checkbox-custom checkbox-success">
                                                        <input type="checkbox" value="Ya" id="checkboxExample3" name="sakit_menahun">
                                                        <label for="checkboxExample3">SAKIT MENAHUN/KRONIS</label>
                                                    </div>

                                                    <div class="checkbox-custom checkbox-warning">
                                                        <input type="checkbox" value="Ya" id="checkboxExample4" name="keluarga_miskin">
                                                        <label for="checkboxExample4">KELUARGA YG BELUM TERIMA JPS LAINNYA</label>
                                                    </div>

                                                    <div class="checkbox-custom checkbox-danger">
                                                        <input type="checkbox" value="Ya" id="checkboxExample5" name="covid">
                                                        <label for="checkboxExample5">KELUARGA MISKIN TERDAMPAK COVID-19</label>
                                                    </div>
                                                    <div class="checkbox-custom">
                                                        <input disabled type="checkbox" value="Ya" id="checkboxExample6" class="lanjut_usia" name="lanjut_usia">
                                                        <label for="checkboxExample6">ANGGOTA KK TUNGGAL LANJUT USIA</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <label class=""">Menerima BLT :</label>
                                                <div class="col-lg-12">
                                                    <div class="radio-custom radio-success">
                                                        <input type="radio" id="radioExample1" value="Ya" name="blt">
                                                        <label for="radioExample1">Ya</label>
                                                    </div>

                                                    <div class="radio-custom radio-danger">
                                                        <input type="radio" id="radioExample2" value="Tidak" name="blt">
                                                        <label for="radioExample2">Tidak</label>
                                                    </div>

                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-end">
                                                    <button type="submit" value="add" name="add" class="btn btn-primary">Submit</button>
                                                    <button class="btn btn-default modal-dismiss">Cancel</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </form>
                                </section>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            {{-- <a class="btn btn-danger" href="">Kosongkan <i class="fas fa-trash"></i></a> --}}
                            <a href="#modalAnimDel" class="modal-with-move-anim ws-normal btn btn-danger">Hapus <i class="fas fa-trash"></i></a>
                        </div>
                        <div class="col-sm-8">
                            <form action="" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fas fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Change</span>
                                            <span class="fileupload-new">Select file</span>
                                            <input type="file" name="file" />
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        <button type="submit" name="import" value="import" class="btn btn-warning">Import <i class="fas fa-database"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                    <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                        <thead>
                            <tr>
                                <th rowspan="2">No.</th>
                                <th rowspan="2">Nama</th>
                                <th rowspan="2">NIK</th>
                                <th style="text-align: center" colspan="6">Kriteria Calon Penerima BLT</th>
                                <th rowspan="2">Menerima BLT</th>
                                <th rowspan="2">Aksi</th>
                            </tr>
                            <tr>
                                <td>Miskin Ekstrim</td>
                                <td>Kehilangan Mata Pencaharian</td>
                                <td>Sakit Menahun/Kronis</td>
                                <td>Keluarga yg belum terima JPS lainya</td>
                                <td>Keluarga Miskin Terdampak Covid-19</td>
                                <td>Anggota KK tunggal lanjut usia</td>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $no=1;    
                            @endphp
                            @foreach ($training as $u)
                            <tr style="text-align: center">                                    
                                <td>{{ $no++ }}</td>
                                <td>{{ $u->nama_anggota_keluarga }}</td>
                                <td>{{ $u->nik }}</td>
                                <td>
                                    @if ($u->miskin_ekstrim=='Ya')
                                        <img src="{{ asset('logo/check.png') }}" width="40%" alt="">
                                    @endif
                                <td>
                                    @if ($u->mata_pencaharian=='Ya')
                                        <img src="{{ asset('logo/check.png') }}" width="40%" alt="">
                                    @endif
                                </td>
                                <td>
                                    @if ($u->sakit_menahun=='Ya')
                                        <img src="{{ asset('logo/check.png') }}" width="40%" alt="">
                                    @endif
                                </td>
                                <td>
                                    @if ($u->keluarga_miskin=='Ya')
                                        <img src="{{ asset('logo/check.png') }}" width="40%" alt="">
                                    @endif
                                </td>
                                <td>
                                    @if ($u->covid=='Ya')
                                        <img src="{{ asset('logo/check.png') }}" width="40%" alt="">
                                    @endif
                                </td>
                                <td>
                                    @if ($u->lanjut_usia=='Ya')
                                        <img src="{{ asset('logo/check.png') }}" width="40%" alt="">
                                    @endif
                                </td>

                                <td>{{ $u->blt }}</td>
                               
                                <td>
                                    {{-- <a href="data-banjar-edit?user={{ $u->id }}" class="btn btn-primary">Edit <i class="bx bx-edit"></i></a>| --}}
                                    <a  onclick="editbtn({{ $u->id }})" href="#editForm" class="modal-with-form btn btn-primary btn btn-primary">Edit <i class="bx bx-edit"></i></a>|
                                    {{-- <button value="{{ $u->id }}" onclick="editbtn({{ $u->id }})" id="#editForm" class="modal-with-form btn btn-primary btn btn-primary">Edit <i class="bx bx-edit"></i></button> --}}
                                    <a onclick="del({{ $u->id }})" href="#modalAnim" class="modal-with-move-anim ws-normal btn btn-warning" data-category="{{ $u->id }}">Hapus <i class="bx bx-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
<!-- end: page -->

<!-- Modal Animation -->
<div id="modalAnim" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Are you sure?</h2>
        </header>
        <form action="{{ route('del.data.training') }}" method="POST">
            @csrf
            <div class="card-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fas fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p class="mb-0">Apakah anda yakin menghapus data ini?</p>
                        <input type="hidden" id="delBanjar" name="id">
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-end">
                        <button type="submit" class="btn btn-primary">Confirm</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </form>
    </section>
</div>
<!-- Modal Animation -->

{{-- modal update --}}
<!-- Modal Form -->
<div id="editForm" class="modal-block modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Form Tambah {{ $title }}</h2>
        </header>
        <form method="POST" action="">
            @csrf
            <input type="hidden" name="id" id="editId">
            <div class="card-body">
                <div class="card-body"> 
                    <div class="form-row">
                        <label>No. KK :</label>
                        <div class="col-lg-12">
                            <select data-plugin-selectTwo class="form-control populate" id="nik" name="nik" ">
                                <option disabled value="">Pilih No. KK - NIK - Nama</option>
                                @foreach ($datakk as $i)
                                <option disabled {{ old('nik')== $i->nik ? 'selected':''}} value="{{ $i->nik }}">{{ $i->kode_keluarga .' - '.$i->nik .' - '. $i->nama_anggota_keluarga}}</option>
                                @endforeach
                            </select>
                            {{-- <input type="hidden" name="kode_keluarga" id="input_kode_keluarga"> --}}
                        </div>
                    </div>
                    <div class="form-row">
                        <label class=""">Checkboxes :</label>
                        <div class="col-lg-12">
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" id="checkboxExample1" class="miskin_ekstrim" value="Ya" name="miskin_ekstrim">
                                <label for="checkboxExample1">MISKIN EKSTRIM</label>
                            </div>

                            <div class="checkbox-custom checkbox-primary">
                                <input type="checkbox" id="checkboxExample2" class="mata_pencaharian" value="Ya" name="mata_pencaharian">
                                <label for="checkboxExample2">KEHILANGAN MATA PENCAHARIAN</label>
                            </div>

                            <div class="checkbox-custom checkbox-success">
                                <input type="checkbox" value="Ya" id="checkboxExample3" class="sakit_menahun" name="sakit_menahun">
                                <label for="checkboxExample3">SAKIT MENAHUN/KRONIS</label>
                            </div>

                            <div class="checkbox-custom checkbox-warning">
                                <input type="checkbox" value="Ya" id="checkboxExample4" class="keluarga_miskin" name="keluarga_miskin">
                                <label for="checkboxExample4">KELUARGA YG BELUM TERIMA JPS LAINNYA</label>
                            </div>

                            <div class="checkbox-custom checkbox-danger">
                                <input type="checkbox" value="Ya" id="checkboxExample5" class="covid" name="covid">
                                <label for="checkboxExample5">KELUARGA MISKIN TERDAMPAK COVID-19</label>
                            </div>
                            <div class="checkbox-custom">
                                <input type="checkbox" value="Ya" id="checkboxExample6" class="lanjut_usia" name="lanjut_usia">
                                <label for="checkboxExample6">ANGGOTA KK TUNGGAL LANJUT USIA</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class=""">Menerima BLT :</label>
                        <div class="col-lg-12">
                            <div class="radio-custom radio-success">
                                <input type="radio" id="radioExample1" class="bltYa" value="Ya" name="blt">
                                <label for="radioExample1">Ya</label>
                            </div>

                            <div class="radio-custom radio-danger">
                                <input type="radio" id="radioExample2" class="bltTidak" value="Tidak" name="blt">
                                <label for="radioExample2">Tidak</label>
                            </div>

                        </div>
                    </div>
                    
                    
                </div>
                <footer class="card-footer">
                    <div class="row">
                        <div class="col-md-12 text-end">
                            <button type="submit" value="edit" name="edit" class="btn btn-primary">Submit</button>
                            <button class="btn btn-default modal-dismiss">Cancel</button>
                        </div>
                    </div>
                </footer>
            </div>
        </form>
    </section>
</div>

{{-- hapus semua data --}}
{{-- kosongkan data --}}

<div id="modalAnimDel" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Konfirmasi</h2>
        </header>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fas fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p class="mb-0">Apakah anda yakin mengkosongkan semua data ini?</p>
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-end">
                        <button type="submit" name="truncate" value="truncate" class="btn btn-primary">Confirm</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </form>
    </section>
</div>

<script>
    function editbtn(id){
        var input = document.getElementById("editId");
        input.value = id;

        // $('#editForm').modal('show');
        $.ajax({
            type: "GET",
            url: "/data-training-edit/"+id,
            success: function (response) {
                // console.log(response.training.kode_keluarga);
                $('#nik').val(response.training.nik).change();
               
                if(response.training.miskin_ekstrim =='Ya'){
                    $('.miskin_ekstrim').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.miskin_ekstrim').each(function(i) {
                        this.checked = false;
                    });
                }

                if(response.training.mata_pencaharian =='Ya'){
                    $('.mata_pencaharian').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.mata_pencaharian').each(function(i) {
                        this.checked = false;
                    });
                }

                if(response.training.sakit_menahun =='Ya'){
                    $('.sakit_menahun').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.sakit_menahun').each(function(i) {
                        this.checked = false;
                    });
                }

                if(response.training.keluarga_miskin =='Ya'){
                    $('.keluarga_miskin').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.keluarga_miskin').each(function(i) {
                        this.checked = false;
                    });
                }

                if(response.training.covid =='Ya'){
                    $('.covid').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.covid').each(function(i) {
                        this.checked = false;
                    });
                }

                if(response.training.lanjut_usia =='Ya'){
                    $('.lanjut_usia').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.lanjut_usia').each(function(i) {
                        this.checked = false;
                    });
                }

                if(response.training.blt =='Ya'){
                    $('.bltYa').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.bltTidak').each(function(i) {
                        this.checked = true;
                    });
                }


                
               
            }
        });
        // alert(id);
        // $('#modalUpdate').modal('show');
    }
</script>


{{-- Skrip tarik data modal confirmation --}}
<script>
    function del(id) {
        // $('#modal-category_name').html(id);
        var input = document.getElementById("delBanjar");
        input.value = id;

        
        // console.log(id);
        // $('#modal-confirm_delete').attr('onclick', 'confirmDelete(${id})');
        // $('#modalAnim').modal('show');
    }

    
</script>

<script>
    $(function() {
        $(document).ready(function() {
            $("select.selectVal").change(function() {
                let selectedItem = $(this).children("option:selected").val();
    
                $.ajax({
                type: "GET",
                url: "/uji-data-baru-cek/"+ selectedItem,
                success: function (response) {
                    console.log(response.cek.usia);
                    if(response.cek.usia>=60){
                        $('.lanjut_usia').each(function(i) {
                            this.checked = true;
                        });
                    }else{
                        $('.lanjut_usia').each(function(i) {
                            this.checked = false;
                        });
                    }
                }
    
                // alert("You have selected the name - " + selectedItem);
                // console.log(selectedItem);
              });
          });
        });
    });
      </script>

@endsection
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>



@push('js')
<!-- Specific Page Vendor -->
<script src="{{ asset('vendor/select2/js/select2.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js')}}"></script>

<script src="{{ asset('vendor/autosize/autosize.js')}}"></script>
<script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js')}}"></script>
@endpush

@push('example')

<!-- Examples -->
<script src="{{ asset('js/examples/examples.datatables.default.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.row.with.details.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.tabletools.js')}}"></script>

<!-- Examples -->
<script>
(function($) {

'use strict';



$('.modal-with-move-anim').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,
    
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom',
    modal: true
});

/*
Modal Dismiss
*/
$(document).on('click', '.modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
});

/*
Modal Confirm
*/
$(document).on('click', '.modal-confirm', function (e) {
    e.preventDefault();
    $.magnificPopup.close();

    const del = document.querySelector("#delete");

    // console.log(del.dataset);
    // console.log('haii');
});

/*
Form
*/
$('.modal-with-form').magnificPopup({
    type: 'inline',
    preloader: false,
    focus: '#name',
    modal: true,

    // When elemened is focused, some mobile browsers in some cases zoom in
    // It looks not nice, so we disable it:
    callbacks: {
        beforeOpen: function() {
            if($(window).width() < 700) {
                this.st.focus = false;
            } else {
                this.st.focus = '#name';
            }
        }
    }
});

/*
Ajax
*/
$('.simple-ajax-modal').magnificPopup({
    type: 'ajax',
    modal: true
});

}).apply(this, [jQuery]);
</script>
@endpush