@extends('layouts.index')

@section('content')
<!-- start: page -->

<div class="row">
    <div class="col-lg-4 col-xl-3 mb-4 mb-xl-0">

        <section class="card">
            <div class="card-body">
                <div class="thumb-info mb-3">
                    <img src="{{ asset('logo/profile.png') }}" class="rounded img-fluid" alt="John Doe">
                    <div class="thumb-info-title">
                        <span class="thumb-info-inner">STAFF</span>
                        <span class="thumb-info-type">DESA</span>
                    </div>
                </div>

                <h5 class="mb-2 mt-3">About</h5>
                <p class="text-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis vulputate quam. Interdum et malesuada</p>
                

                <hr class="dotted short">

                <div class="social-icons-list">
                    <a rel="tooltip" data-bs-placement="bottom" target="_blank" href="http://www.facebook.com" data-original-title="Facebook"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
                    <a rel="tooltip" data-bs-placement="bottom" href="http://www.twitter.com" data-original-title="Twitter"><i class="fab fa-twitter"></i><span>Twitter</span></a>
                    <a rel="tooltip" data-bs-placement="bottom" href="http://www.linkedin.com" data-original-title="Linkedin"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
                </div>

            </div>
        </section>

        

    </div>
    <div class="col-lg-8 col-xl-6">

        <div class="tabs">
            <ul class="nav nav-tabs tabs-primary">
                <li class="nav-item active">
                    <button class="nav-link" data-bs-target="#overview" data-bs-toggle="tab">Overview</button>
                </li>
            </ul>
            <div class="tab-content">
                <div id="overview" class="tab-pane active">

                    <div class="p-3">

                        <h4 class="mb-3 pt-4 font-weight-semibold text-dark">Timeline</h4>

                        <div class="timeline timeline-simple mt-3 mb-3">
                            <div class="tm-body">
                                <div class="tm-title">
                                    <h5 class="m-0 pt-2 pb-2 text-dark font-weight-semibold text-uppercase">November 2021</h5>
                                </div>
                                <ol class="tm-items">
                                    <li>
                                        <div class="tm-box">
                                            <p class="text-muted mb-0">7 months ago.</p>
                                            <p>
                                                It's awesome when we find a good solution for our projects, Porto Admin is <span class="text-primary">#awesome</span>
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="tm-box">
                                            <p class="text-muted mb-0">7 months ago.</p>
                                            <p>
                                                What is your biggest developer pain point?
                                            </p>
                                        </div>
                                    </li>
                              
                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3">

        <h4 class="mb-3 mt-0 font-weight-semibold text-dark">Sale Stats</h4>
        <ul class="simple-card-list mb-3">
            <li class="primary">
                <h3>488</h3>
                <p class="text-light">Nullam quris ris.</p>
            </li>
            <li class="primary">
                <h3>$ 189,000.00</h3>
                <p class="text-light">Nullam quris ris.</p>
            </li>
            <li class="primary">
                <h3>16</h3>
                <p class="text-light">Nullam quris ris.</p>
            </li>
        </ul>

        <h4 class="mb-3 mt-4 pt-2 font-weight-semibold text-dark">Projects</h4>
        <ul class="simple-bullet-list mb-3">
            <li class="red">
                <span class="title">Porto Template</span>
                <span class="description truncate">Lorem ipsom dolor sit.</span>
            </li>
            <li class="green">
                <span class="title">Tucson HTML5 Template</span>
                <span class="description truncate">Lorem ipsom dolor sit amet</span>
            </li>
            <li class="blue">
                <span class="title">Porto HTML5 Template</span>
                <span class="description truncate">Lorem ipsom dolor sit.</span>
            </li>
            <li class="orange">
                <span class="title">Tucson Template</span>
                <span class="description truncate">Lorem ipsom dolor sit.</span>
            </li>
        </ul>


    </div>

</div>
<!-- end: page -->
@endsection

@push('js')
    <!-- Specific Page Vendor -->
	<script src="vendor/autosize/autosize.js"></script>
@endpush