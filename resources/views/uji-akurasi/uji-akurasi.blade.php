@extends('layouts.index')
@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap5.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />

@endpush
@section('content')
<!-- start: page -->
<div class="row">
    <div class="col">
        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>

                <h2 class="card-title">{{ $title }}</h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <form method="POST" action="">
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <label class="">Presentase Testing :</label>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="Presentase Testing" name="persentase" onkeypress="return hanyaAngka(event)" >
                                        </div>
                                        <div class="d-md-none mb-2"></div>
                                        <div class="col-lg-1">
                                            <input type="text" disabled class="form-control" value="%">
                                        </div>
                                    </div>
                                    <small><cite title="Magazine X">Masukkan presentase testing dari 20 sampai 60 persen</cite></small> 
                                </div>
                            </div>
                            <div class="col-lg-4 mt-2">
                                <label >Data Testing :</label>
                                <select class="form-control" name="data_testing">
                                    <option value="">Pilih Data Testing</option>
                                    <option value="Urut">Urut</option>
                                    <option value="Acak">Acak</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row mt-2">
                            <div class="col-lg-6 text-left">
                                <button type="submit" name="count" value="count" class="btn btn-primary">Hitung</button>
                            </div>
                        </div>
                    </form> 
                </div>
                <br>

                
                @if(!empty($classPro))
                <small style="font-weight: 900">Berikut {{ $persentase }}% data yg dipilih secara {{ $data_testing }}</small> 
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="font-weight-bold text-dark">Class Probabilitas</h4>
                        <div class="toggle toggle-primary" data-plugin-toggle>
                            <section class="toggle">
                                <label>Class Probabilitas</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered table-striped mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th colspan="4">Keterangan</th>
                                            </tr>
                                            <tr>
                                                <th colspan="4">P(Ci)</th>
                                            </tr>
                                            <tr>
                                                <th colspan="2">P(Layak)</th>
                                                <th colspan="2">P(Tidak Layak)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Layak</td>
                                                <td>{{ $classPro['ya'].'/'.$classPro['total'] }}</td>
                                                <td>Tidak Layak</td>
                                                <td>{{ $classPro['tidak'].'/'.$classPro['total'] }}</td>
                                            </tr> 
                                            <tr>
                                                <td>Layak</td>
                                                <td>{{ $classPro['ya_total'] }}</td>
                                                <td>Tidak Layak</td>
                                                <td>{{ $classPro['tidak_total'] }}</td>
                                            </tr> 
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                @endif

                @if(!empty($condPro))
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="font-weight-bold text-dark">Menghitung Conditional Probabilitas</h4>
                        <div class="toggle toggle-primary" data-plugin-toggle>
                            <section class="toggle">
                                <label>Nilai Dari Miskin Ekstrim</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered table-striped mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th rowspan="">Miskin Ekstrim</th>
                                                <th colspan="2">Jumlah Kejadian</th>
                                                <th colspan="2">Probabilitas</th>
                                            </tr>
                                            <tr>
                                                <th>Keterangan</th>
                                                <th>Menerima</th>
                                                <th>Tidak Menerima</th>
                                                <th>Menerima (C1)</th>
                                                <th>Tidak Menerima (C0)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Ya</td>
                                                <td>{{ $condPro['miskin_ekstrim']['m_y'] }}</td>
                                                <td>{{ $condPro['miskin_ekstrim']['tm_y'] }}</td>
                                                <td>{{ $condPro['miskin_ekstrim']['p_m_y'] }}</td>
                                                <td>{{ $condPro['miskin_ekstrim']['p_tm_y'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tidak</td>
                                                <td>{{ $condPro['miskin_ekstrim']['m_t'] }}</td>
                                                <td>{{ $condPro['miskin_ekstrim']['tm_t'] }}</td>
                                                <td>{{ $condPro['miskin_ekstrim']['p_m_t'] }}</td>
                                                <td>{{ $condPro['miskin_ekstrim']['p_tm_t'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Jumlah</td>
                                                <td>{{ $condPro['miskin_ekstrim']['j_m'] }}</td>
                                                <td>{{ $condPro['miskin_ekstrim']['j_tm'] }}</td>
                                                <td>{{ $condPro['miskin_ekstrim']['j_p_m'] }}</td>
                                                <td>{{ $condPro['miskin_ekstrim']['j_p_tm'] }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            <section class="toggle">
                                <label>Nilai Dari Mata Pencaharian</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered table-striped mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th rowspan="">Mata Pencaharian</th>
                                                <th colspan="2">Jumlah Kejadian</th>
                                                <th colspan="2">Probabilitas</th>
                                            </tr>
                                            <tr>
                                                <th>Keterangan</th>
                                                <th>Menerima</th>
                                                <th>Tidak Menerima</th>
                                                <th>Menerima (C1)</th>
                                                <th>Tidak Menerima (C0)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Ya</td>
                                                <td>{{ $condPro['mata_pencaharian']['m_y'] }}</td>
                                                <td>{{ $condPro['mata_pencaharian']['tm_y'] }}</td>
                                                <td>{{ $condPro['mata_pencaharian']['p_m_y'] }}</td>
                                                <td>{{ $condPro['mata_pencaharian']['p_tm_y'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tidak</td>
                                                <td>{{ $condPro['mata_pencaharian']['m_t'] }}</td>
                                                <td>{{ $condPro['mata_pencaharian']['tm_t'] }}</td>
                                                <td>{{ $condPro['mata_pencaharian']['p_m_t'] }}</td>
                                                <td>{{ $condPro['mata_pencaharian']['p_tm_t'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Jumlah</td>
                                                <td>{{ $condPro['mata_pencaharian']['j_m'] }}</td>
                                                <td>{{ $condPro['mata_pencaharian']['j_tm'] }}</td>
                                                <td>{{ $condPro['mata_pencaharian']['j_p_m'] }}</td>
                                                <td>{{ $condPro['mata_pencaharian']['j_p_tm'] }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            <section class="toggle">
                                <label>Nilai Dari Sakit Menahun</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered table-striped mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th rowspan="">Sakit Menahun</th>
                                                <th colspan="2">Jumlah Kejadian</th>
                                                <th colspan="2">Probabilitas</th>
                                            </tr>
                                            <tr>
                                                <th>Keterangan</th>
                                                <th>Menerima</th>
                                                <th>Tidak Menerima</th>
                                                <th>Menerima (C1)</th>
                                                <th>Tidak Menerima (C0)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Ya</td>
                                                <td>{{ $condPro['sakit_menahun']['m_y'] }}</td>
                                                <td>{{ $condPro['sakit_menahun']['tm_y'] }}</td>
                                                <td>{{ $condPro['sakit_menahun']['p_m_y'] }}</td>
                                                <td>{{ $condPro['sakit_menahun']['p_tm_y'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tidak</td>
                                                <td>{{ $condPro['sakit_menahun']['m_t'] }}</td>
                                                <td>{{ $condPro['sakit_menahun']['tm_t'] }}</td>
                                                <td>{{ $condPro['sakit_menahun']['p_m_t'] }}</td>
                                                <td>{{ $condPro['sakit_menahun']['p_tm_t'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Jumlah</td>
                                                <td>{{ $condPro['sakit_menahun']['j_m'] }}</td>
                                                <td>{{ $condPro['sakit_menahun']['j_tm'] }}</td>
                                                <td>{{ $condPro['sakit_menahun']['j_p_m'] }}</td>
                                                <td>{{ $condPro['sakit_menahun']['j_p_tm'] }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            <section class="toggle">
                                <label>Nilai Dari Keluarga Miskin</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered table-striped mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th rowspan="">Keluarga Miskin</th>
                                                <th colspan="2">Jumlah Kejadian</th>
                                                <th colspan="2">Probabilitas</th>
                                            </tr>
                                            <tr>
                                                <th>Keterangan</th>
                                                <th>Menerima</th>
                                                <th>Tidak Menerima</th>
                                                <th>Menerima (C1)</th>
                                                <th>Tidak Menerima (C0)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Ya</td>
                                                <td>{{ $condPro['keluarga_miskin']['m_y'] }}</td>
                                                <td>{{ $condPro['keluarga_miskin']['tm_y'] }}</td>
                                                <td>{{ $condPro['keluarga_miskin']['p_m_y'] }}</td>
                                                <td>{{ $condPro['keluarga_miskin']['p_tm_y'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tidak</td>
                                                <td>{{ $condPro['keluarga_miskin']['m_t'] }}</td>
                                                <td>{{ $condPro['keluarga_miskin']['tm_t'] }}</td>
                                                <td>{{ $condPro['keluarga_miskin']['p_m_t'] }}</td>
                                                <td>{{ $condPro['keluarga_miskin']['p_tm_t'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Jumlah</td>
                                                <td>{{ $condPro['keluarga_miskin']['j_m'] }}</td>
                                                <td>{{ $condPro['keluarga_miskin']['j_tm'] }}</td>
                                                <td>{{ $condPro['keluarga_miskin']['j_p_m'] }}</td>
                                                <td>{{ $condPro['keluarga_miskin']['j_p_tm'] }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            <section class="toggle">
                                <label>Nilai Dari Covid</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered table-striped mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th rowspan="">Covid</th>
                                                <th colspan="2">Jumlah Kejadian</th>
                                                <th colspan="2">Probabilitas</th>
                                            </tr>
                                            <tr>
                                                <th>Keterangan</th>
                                                <th>Menerima</th>
                                                <th>Tidak Menerima</th>
                                                <th>Menerima (C1)</th>
                                                <th>Tidak Menerima (C0)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Ya</td>
                                                <td>{{ $condPro['covid']['m_y'] }}</td>
                                                <td>{{ $condPro['covid']['tm_y'] }}</td>
                                                <td>{{ $condPro['covid']['p_m_y'] }}</td>
                                                <td>{{ $condPro['covid']['p_tm_y'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tidak</td>
                                                <td>{{ $condPro['covid']['m_t'] }}</td>
                                                <td>{{ $condPro['covid']['tm_t'] }}</td>
                                                <td>{{ $condPro['covid']['p_m_t'] }}</td>
                                                <td>{{ $condPro['covid']['p_tm_t'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Jumlah</td>
                                                <td>{{ $condPro['covid']['j_m'] }}</td>
                                                <td>{{ $condPro['covid']['j_tm'] }}</td>
                                                <td>{{ $condPro['covid']['j_p_m'] }}</td>
                                                <td>{{ $condPro['covid']['j_p_tm'] }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            <section class="toggle">
                                <label>Nilai Dari Lanjut Usia</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered table-striped mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th rowspan="">Lanjut Usia</th>
                                                <th colspan="2">Jumlah Kejadian</th>
                                                <th colspan="2">Probabilitas</th>
                                            </tr>
                                            <tr>
                                                <th>Keterangan</th>
                                                <th>Menerima</th>
                                                <th>Tidak Menerima</th>
                                                <th>Menerima (C1)</th>
                                                <th>Tidak Menerima (C0)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Ya</td>
                                                <td>{{ $condPro['lanjut_usia']['m_y'] }}</td>
                                                <td>{{ $condPro['lanjut_usia']['tm_y'] }}</td>
                                                <td>{{ $condPro['lanjut_usia']['p_m_y'] }}</td>
                                                <td>{{ $condPro['lanjut_usia']['p_tm_y'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tidak</td>
                                                <td>{{ $condPro['lanjut_usia']['m_t'] }}</td>
                                                <td>{{ $condPro['lanjut_usia']['tm_t'] }}</td>
                                                <td>{{ $condPro['lanjut_usia']['p_m_t'] }}</td>
                                                <td>{{ $condPro['lanjut_usia']['p_tm_t'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>Jumlah</td>
                                                <td>{{ $condPro['lanjut_usia']['j_m'] }}</td>
                                                <td>{{ $condPro['lanjut_usia']['j_tm'] }}</td>
                                                <td>{{ $condPro['lanjut_usia']['j_p_m'] }}</td>
                                                <td>{{ $condPro['lanjut_usia']['j_p_tm'] }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                @endif
                @if(!empty($testing))
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="font-weight-bold text-dark">Perhitungan</h4>
                        <div class="toggle toggle-primary" data-plugin-toggle>
                            <section class="toggle">
                                <label>Data Testing</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered table-striped mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th rowspan="2">No.</th>
                                                <th rowspan="2">Nama</th>
                                                <th rowspan="2">NIK</th>
                                                <th style="text-align: center" colspan="6">Kriteria Calon Penerima BLT</th>
                                                <th rowspan="2">Menerima BLT</th>
                                            </tr>
                                            <tr>
                                                <td>Miskin Ekstrim</td>
                                                <td>Kehilangan Mata Pencaharian</td>
                                                <td>Sakit Menahun/Kronis</td>
                                                <td>Keluarga yg belum terima JPS lainya</td>
                                                <td>Keluarga Miskin Terdampak Covid-19</td>
                                                <td>Anggota KK tunggal lanjut usia</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no=1;    
                                            @endphp
                                            @foreach ($testing as $u)
                                            <tr style="text-align: center">                                    
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $u->nama_anggota_keluarga }}</td>
                                                <td>{{ $u->nik }}</td>
                                                <td>
                                                    @if ($u->miskin_ekstrim=='Ya')
                                                        <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                                    @endif
                                                <td>
                                                    @if ($u->mata_pencaharian=='Ya')
                                                        <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($u->sakit_menahun=='Ya')
                                                        <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($u->keluarga_miskin=='Ya')
                                                        <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($u->covid=='Ya')
                                                        <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($u->lanjut_usia=='Ya')
                                                        <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                                    @endif
                                                </td>
                
                                                <td>{{ $u->blt }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            {{-- <section class="toggle">
                                <label>Normalisasi</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered table-striped mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th rowspan="2">No.</th>
                                                <th rowspan="2">Nama</th>
                                                <th rowspan="2">NIK</th>
                                                <th style="text-align: center" colspan="6">Kriteria Calon Penerima BLT</th>
                                                <th rowspan="2">Menerima BLT</th>
                                            </tr>
                                            <tr>
                                                <td>Miskin Ekstrim</td>
                                                <td>Kehilangan Mata Pencaharian</td>
                                                <td>Sakit Menahun/Kronis</td>
                                                <td>Keluarga yg belum terima JPS lainya</td>
                                                <td>Keluarga Miskin Terdampak Covid-19</td>
                                                <td>Anggota KK tunggal lanjut usia</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no=1;  
                                            // dd($normalisasi);  
                                            @endphp
                                            
                                            @foreach ($normalisasi as $u)
                                            <tr style="text-align: center">                                    
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $u['nama_anggota_keluarga'] }}</td>
                                                <td>{{ $u['nik'] }}</td>                
                                                <td>{{ $u['miskin_ekstrim'] }}</td>                
                                                <td>{{ $u['mata_pencaharian'] }}</td>                
                                                <td>{{ $u['sakit_menahun'] }}</td>                
                                                <td>{{ $u['keluarga_miskin'] }}</td>                
                                                <td>{{ $u['covid'] }}</td>                
                                                <td>{{ $u['lanjut_usia'] }}</td>                
                                                <td>{{ $u['blt'] }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </section> --}}
                            <section class="toggle">
                                <label>Perhitungan</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th style="text-align: center" colspan="6">Hasil Proses Perhitungan</th>
                                            </tr>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>NIK</th>
                                                <th colspan="2">Proses</th>
                                                <th>Hasil</th>
                                            </tr>
                                           
                                        </thead>
                                        <tbody>
                                            @php
                                            $no=1;  
                                            @endphp
                                            @foreach ($perhitungan as $p)
                                            <tr>
                                                <td rowspan="2">{{ $no++ }}</td>
                                                <td rowspan="2">{{ $p['nama_anggota_keluarga'] }}</td>
                                                <td rowspan="2">{{ $p['nik'] }}</td>
                                                <td >P(C1|X)=</td>
                                                <td >{{ $p['c1'] }}</td>
                                                <td rowspan="2">{{ $p['blt'] }}</td>
                                            </tr>
                                            <tr>
                                                <td>P(C0|X)=</td>
                                                <td>{{ $p['c0'] }}</td>
                                                
                                            </tr>
                                            @endforeach
                                           
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                @endif

                @if(!empty($klas))
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="font-weight-bold text-dark">Confusion Matrix</h4>
                        <div class="toggle toggle-primary" data-plugin-toggle>
                            <section class="toggle">
                                <label>Klasifikasi</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered table-striped mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama</th>
                                                <th>NIK</th>
                                                <th>Actual</th>
                                                <th>Predict</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no=1;    
                                            @endphp
                                            @foreach ($klas as $u)
                                            <tr style="text-align: center">  
                                                <td>{{ $no++ }}</td>                                  
                                               <td>{{ $u['nama_anggota_keluarga'] }}</td>
                                               <td>{{ $u['nik'] }}</td>
                                               <td>{{ $u['blt'] }}</td>
                                               <td>{{ $u['bltact'] }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            <section class="toggle">
                                <label>Confusion Matrix</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th colspan="2" rowspan="2" >Klasifikasi</th>
                                                <th colspan="2">Actual</th>
                                            </tr>
                                            <tr>
                                                <th>Menerima</th>
                                                <th>Tidak Menerima</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no=1;    
                                            @endphp
                                            <tr>
                                                <td rowspan="2" style="rotate: 270deg; text-align: center; font-weight: 700">Predict</td>
                                                <td>Menerima</td>
                                                <td>{{ $conf['tp'] }}</td>
                                                <td>{{ $conf['fp'] }}</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Tidak Menerima</td>
                                                <td>{{ $conf['fn'] }}</td>
                                                <td>{{ $conf['tn'] }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                @endif

                @if(!empty($klas))
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="font-weight-bold text-dark">Hasil</h4>
                        <div class="toggle toggle-primary" data-plugin-toggle>

                            <section class="toggle">
                                <label>Hasil</label>
                                <div class="toggle-content">
                                    <table class="table table-bordered table-striped mb-0">
                                        <thead style="text-align: center">
                                            <tr>
                                                <th>Data Testing</th>
                                                <th>Accuracy</th>
                                                <th>Precision</th>
                                                <th>Recall</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $persentase }} % secara {{ $data_testing }}</td>
                                                <td>{{ $hasil['acc'] }} %</td>
                                                <td>{{ $hasil['pre'] }} %</td>
                                                <td>{{ $hasil['rec'] }} %</td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                @endif



            </div>
            
        </section>
    </div>
</div>


<!-- end: page -->

@endsection



@push('js')
<!-- Specific Page Vendor -->
<script src="{{ asset('vendor/select2/js/select2.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js')}}"></script>


<script src="{{ asset('vendor/autosize/autosize.js')}}"></script>
<script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js')}}"></script>
@endpush

@push('example')

<!-- Examples -->
<script src="{{ asset('js/examples/examples.datatables.default.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.row.with.details.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.tabletools.js')}}"></script>

@endpush