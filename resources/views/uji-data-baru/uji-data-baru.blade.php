@extends('layouts.index')
@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap5.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />

    <style>
        tr,th{
            vertical-align: middle;
        }
    </style>
@endpush
@section('content')
    <!-- start: page -->
    <div class="row">
        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">{{ $title }} Desa Buahan Kaja</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2">
                            {{-- <button id="modalForm"  class="btn btn-primary">Tambah <i class="fas fa-plus"></i></button> --}}
                            <a class="modal-with-form btn btn-primary" href="#modalForm">Tambah <i class="fas fa-plus"></i></a>
                            <!-- Modal Form -->
                            <div id="modalForm" class="modal-block modal-block-primary mfp-hide">
                                <section class="card">
                                    <header class="card-header">
                                        <h2 class="card-title">Form Tambah {{ $title }}</h2>
                                    </header>
                                    <form method="POST" action="">
                                        @csrf
                                        <div class="card-body"> 
                                            <div class="form-row">
                                                <label>No. KK :</label>
                                                <div class="col-lg-12">
                                                    <select data-plugin-selectTwo class="form-control populate selectVal" name="nik" required >
                                                        <option value="" >Pilih No. KK - NIK - Nama</option>
                                                        @foreach ($datakk as $i)
                                                        <option {{ old('nik')== $i->nik ? 'selected':''}} value="{{ $i->nik }}">{{ $i->kode_keluarga .' - '.$i->nik .' - '. $i->nama_anggota_keluarga}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-lg-6">
                                                    <label class=""">No. KK :</label>
                                                    <span class="help-block" id="kk"></span>
                                                    <label class=""">Nama :</label>
                                                    <span class="help-block" id="nama"></span>
												</div>
                                                <label class=""">Checkboxes :</label>
                                                <div class="col-lg-12">
                                                    <div class="checkbox-custom checkbox-default">
                                                        <input type="checkbox" id="checkboxExample1" value="Ya" name="miskin_ekstrim">
                                                        <label for="checkboxExample1">MISKIN EKSTRIM</label>
                                                    </div>

                                                    <div class="checkbox-custom checkbox-primary">
                                                        <input type="checkbox" id="checkboxExample2" value="Ya" name="mata_pencaharian">
                                                        <label for="checkboxExample2">KEHILANGAN MATA PENCAHARIAN</label>
                                                    </div>

                                                    <div class="checkbox-custom checkbox-success">
                                                        <input type="checkbox" value="Ya" id="checkboxExample3" name="sakit_menahun">
                                                        <label for="checkboxExample3">SAKIT MENAHUN/KRONIS</label>
                                                    </div>

                                                    <div class="checkbox-custom checkbox-warning">
                                                        <input type="checkbox" value="Ya" id="checkboxExample4" name="keluarga_miskin">
                                                        <label for="checkboxExample4">KELUARGA YG BELUM TERIMA JPS LAINNYA</label>
                                                    </div>

                                                    <div class="checkbox-custom checkbox-danger">
                                                        <input type="checkbox" value="Ya" id="checkboxExample5" name="covid">
                                                        <label for="checkboxExample5">KELUARGA MISKIN TERDAMPAK COVID-19</label>
                                                    </div>
                                                    <div class="checkbox-custom">
                                                        <input disabled type="checkbox" value="Ya" id="checkboxExample6" class="lanjut_usia" name="lanjut_usia">
                                                        <label for="checkboxExample6">ANGGOTA KK TUNGGAL LANJUT USIA</label>
                                                    </div>
                                                </div>
                                            </div>                                        
                                            
                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-end">
                                                    <button type="submit" value="add" name="add" class="btn btn-primary">Submit</button>
                                                    <button class="btn btn-default modal-dismiss">Cancel</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </form>
                                </section>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            {{-- <a class="btn btn-danger" href="">Kosongkan <i class="fas fa-trash"></i></a> --}}
                            <a href="#modalAnimDel" class="modal-with-move-anim ws-normal btn btn-danger">Hapus <i class="fas fa-trash"></i></a>
                        </div>
                        
                    </div>

                    
                    <table class="table table-bordered table-striped mb-0" id="datatable-tabletools">
                        <thead>
                            <tr>
                                <th rowspan="2">No.</th>
                                <th rowspan="2">Nama</th>
                                <th rowspan="2">NIK</th>
                                <th style="text-align: center" colspan="6">Kriteria Calon Penerima BLT</th>
                                <th rowspan="2">Aksi</th>
                            </tr>
                            <tr>
                                <td>Miskin Ekstrim</td>
                                <td>Kehilangan Mata Pencaharian</td>
                                <td>Sakit Menahun/Kronis</td>
                                <td>Keluarga yg belum terima JPS lainya</td>
                                <td>Keluarga Miskin Terdampak Covid-19</td>
                                <td>Anggota KK tunggal lanjut usia</td>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $no=1;    
                            @endphp
                            @foreach ($ujidata as $u)
                            <tr style="text-align: center">                                    
                                <td>{{ $no++ }}</td>
                                <td>{{ $u->nama_anggota_keluarga }}</td>
                                <td>{{ $u->nik }}</td>
                                <td>
                                    @if ($u->miskin_ekstrim=='Ya')
                                        <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                    @endif
                                <td>
                                    @if ($u->mata_pencaharian=='Ya')
                                        <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                    @endif
                                </td>
                                <td>
                                    @if ($u->sakit_menahun=='Ya')
                                        <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                    @endif
                                </td>
                                <td>
                                    @if ($u->keluarga_miskin=='Ya')
                                        <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                    @endif
                                </td>
                                <td>
                                    @if ($u->covid=='Ya')
                                        <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                    @endif
                                </td>
                                <td>
                                    @if ($u->lanjut_usia=='Ya')
                                        <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                    @endif
                                </td>

                              
                               
                                <td>
                                    {{-- <a href="data-banjar-edit?user={{ $u->id }}" class="btn btn-primary">Edit <i class="bx bx-edit"></i></a>| --}}
                                    <a  onclick="editbtn({{ $u->id }})" href="#editForm" class="modal-with-form btn btn-primary btn btn-primary">Edit <i class="bx bx-edit"></i></a>|
                                    {{-- <button value="{{ $u->id }}" onclick="editbtn({{ $u->id }})" id="#editForm" class="modal-with-form btn btn-primary btn btn-primary">Edit <i class="bx bx-edit"></i></button> --}}
                                    <a onclick="del({{ $u->id }})" href="#modalAnim" class="modal-with-move-anim ws-normal btn btn-warning" data-category="{{ $u->id }}">Hapus <i class="bx bx-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
            </section>
            <section class="card">
                <div class="card-body">
                    <div class="row">
                        <form method="POST" action="">
                            @csrf
                            <div class="form-row">
                                <div class="col-lg-6 text-left">
                                    <button type="submit" name="count" value="count" class="btn btn-primary">Hitung</button>
                                </div>
                            </div>
                        </form> 
                    </div>
                    <br>

                    
                    @if(!empty($classPro))
                    <small style="font-weight: 900">Berikut {{ $persentase }} data yg dipilih</small> 
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="font-weight-bold text-dark">Class Probabilitas</h4>
                            <div class="toggle toggle-primary" data-plugin-toggle>
                                <section class="toggle">
                                    <label>Class Probabilitas</label>
                                    <div class="toggle-content">
                                        <table class="table table-bordered table-striped mb-0">
                                            <thead style="text-align: center">
                                                <tr>
                                                    <th colspan="4">Keterangan</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="4">P(Ci)</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="2">P(Layak)</th>
                                                    <th colspan="2">P(Tidak Layak)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Layak</td>
                                                    <td>{{ $classPro['ya'].'/'.$classPro['total'] }}</td>
                                                    <td>Tidak Layak</td>
                                                    <td>{{ $classPro['tidak'].'/'.$classPro['total'] }}</td>
                                                </tr> 
                                                <tr>
                                                    <td>Layak</td>
                                                    <td>{{ $classPro['ya_total'] }}</td>
                                                    <td>Tidak Layak</td>
                                                    <td>{{ $classPro['tidak_total'] }}</td>
                                                </tr> 
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if(!empty($condPro))
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="font-weight-bold text-dark">Menghitung Conditional Probabilitas</h4>
                            <div class="toggle toggle-primary" data-plugin-toggle>
                                <section class="toggle">
                                    <label>Nilai Dari Miskin Ekstrim</label>
                                    <div class="toggle-content">
                                        <table class="table table-bordered table-striped mb-0">
                                            <thead style="text-align: center">
                                                <tr>
                                                    <th rowspan="">Miskin Ekstrim</th>
                                                    <th colspan="2">Jumlah Kejadian</th>
                                                    <th colspan="2">Probabilitas</th>
                                                </tr>
                                                <tr>
                                                    <th>Keterangan</th>
                                                    <th>Menerima</th>
                                                    <th>Tidak Menerima</th>
                                                    <th>Menerima (C1)</th>
                                                    <th>Tidak Menerima (C0)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Ya</td>
                                                    <td>{{ $condPro['miskin_ekstrim']['m_y'] }}</td>
                                                    <td>{{ $condPro['miskin_ekstrim']['tm_y'] }}</td>
                                                    <td>{{ $condPro['miskin_ekstrim']['p_m_y'] }}</td>
                                                    <td>{{ $condPro['miskin_ekstrim']['p_tm_y'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tidak</td>
                                                    <td>{{ $condPro['miskin_ekstrim']['m_t'] }}</td>
                                                    <td>{{ $condPro['miskin_ekstrim']['tm_t'] }}</td>
                                                    <td>{{ $condPro['miskin_ekstrim']['p_m_t'] }}</td>
                                                    <td>{{ $condPro['miskin_ekstrim']['p_tm_t'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah</td>
                                                    <td>{{ $condPro['miskin_ekstrim']['j_m'] }}</td>
                                                    <td>{{ $condPro['miskin_ekstrim']['j_tm'] }}</td>
                                                    <td>{{ $condPro['miskin_ekstrim']['j_p_m'] }}</td>
                                                    <td>{{ $condPro['miskin_ekstrim']['j_p_tm'] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                                <section class="toggle">
                                    <label>Nilai Dari Mata Pencaharian</label>
                                    <div class="toggle-content">
                                        <table class="table table-bordered table-striped mb-0">
                                            <thead style="text-align: center">
                                                <tr>
                                                    <th rowspan="">Mata Pencaharian</th>
                                                    <th colspan="2">Jumlah Kejadian</th>
                                                    <th colspan="2">Probabilitas</th>
                                                </tr>
                                                <tr>
                                                    <th>Keterangan</th>
                                                    <th>Menerima</th>
                                                    <th>Tidak Menerima</th>
                                                    <th>Menerima (C1)</th>
                                                    <th>Tidak Menerima (C0)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Ya</td>
                                                    <td>{{ $condPro['mata_pencaharian']['m_y'] }}</td>
                                                    <td>{{ $condPro['mata_pencaharian']['tm_y'] }}</td>
                                                    <td>{{ $condPro['mata_pencaharian']['p_m_y'] }}</td>
                                                    <td>{{ $condPro['mata_pencaharian']['p_tm_y'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tidak</td>
                                                    <td>{{ $condPro['mata_pencaharian']['m_t'] }}</td>
                                                    <td>{{ $condPro['mata_pencaharian']['tm_t'] }}</td>
                                                    <td>{{ $condPro['mata_pencaharian']['p_m_t'] }}</td>
                                                    <td>{{ $condPro['mata_pencaharian']['p_tm_t'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah</td>
                                                    <td>{{ $condPro['mata_pencaharian']['j_m'] }}</td>
                                                    <td>{{ $condPro['mata_pencaharian']['j_tm'] }}</td>
                                                    <td>{{ $condPro['mata_pencaharian']['j_p_m'] }}</td>
                                                    <td>{{ $condPro['mata_pencaharian']['j_p_tm'] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                                <section class="toggle">
                                    <label>Nilai Dari Sakit Menahun</label>
                                    <div class="toggle-content">
                                        <table class="table table-bordered table-striped mb-0">
                                            <thead style="text-align: center">
                                                <tr>
                                                    <th rowspan="">Sakit Menahun</th>
                                                    <th colspan="2">Jumlah Kejadian</th>
                                                    <th colspan="2">Probabilitas</th>
                                                </tr>
                                                <tr>
                                                    <th>Keterangan</th>
                                                    <th>Menerima</th>
                                                    <th>Tidak Menerima</th>
                                                    <th>Menerima (C1)</th>
                                                    <th>Tidak Menerima (C0)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Ya</td>
                                                    <td>{{ $condPro['sakit_menahun']['m_y'] }}</td>
                                                    <td>{{ $condPro['sakit_menahun']['tm_y'] }}</td>
                                                    <td>{{ $condPro['sakit_menahun']['p_m_y'] }}</td>
                                                    <td>{{ $condPro['sakit_menahun']['p_tm_y'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tidak</td>
                                                    <td>{{ $condPro['sakit_menahun']['m_t'] }}</td>
                                                    <td>{{ $condPro['sakit_menahun']['tm_t'] }}</td>
                                                    <td>{{ $condPro['sakit_menahun']['p_m_t'] }}</td>
                                                    <td>{{ $condPro['sakit_menahun']['p_tm_t'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah</td>
                                                    <td>{{ $condPro['sakit_menahun']['j_m'] }}</td>
                                                    <td>{{ $condPro['sakit_menahun']['j_tm'] }}</td>
                                                    <td>{{ $condPro['sakit_menahun']['j_p_m'] }}</td>
                                                    <td>{{ $condPro['sakit_menahun']['j_p_tm'] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                                <section class="toggle">
                                    <label>Nilai Dari Keluarga Miskin</label>
                                    <div class="toggle-content">
                                        <table class="table table-bordered table-striped mb-0">
                                            <thead style="text-align: center">
                                                <tr>
                                                    <th rowspan="">Keluarga Miskin</th>
                                                    <th colspan="2">Jumlah Kejadian</th>
                                                    <th colspan="2">Probabilitas</th>
                                                </tr>
                                                <tr>
                                                    <th>Keterangan</th>
                                                    <th>Menerima</th>
                                                    <th>Tidak Menerima</th>
                                                    <th>Menerima (C1)</th>
                                                    <th>Tidak Menerima (C0)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Ya</td>
                                                    <td>{{ $condPro['keluarga_miskin']['m_y'] }}</td>
                                                    <td>{{ $condPro['keluarga_miskin']['tm_y'] }}</td>
                                                    <td>{{ $condPro['keluarga_miskin']['p_m_y'] }}</td>
                                                    <td>{{ $condPro['keluarga_miskin']['p_tm_y'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tidak</td>
                                                    <td>{{ $condPro['keluarga_miskin']['m_t'] }}</td>
                                                    <td>{{ $condPro['keluarga_miskin']['tm_t'] }}</td>
                                                    <td>{{ $condPro['keluarga_miskin']['p_m_t'] }}</td>
                                                    <td>{{ $condPro['keluarga_miskin']['p_tm_t'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah</td>
                                                    <td>{{ $condPro['keluarga_miskin']['j_m'] }}</td>
                                                    <td>{{ $condPro['keluarga_miskin']['j_tm'] }}</td>
                                                    <td>{{ $condPro['keluarga_miskin']['j_p_m'] }}</td>
                                                    <td>{{ $condPro['keluarga_miskin']['j_p_tm'] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                                <section class="toggle">
                                    <label>Nilai Dari Covid</label>
                                    <div class="toggle-content">
                                        <table class="table table-bordered table-striped mb-0">
                                            <thead style="text-align: center">
                                                <tr>
                                                    <th rowspan="">Covid</th>
                                                    <th colspan="2">Jumlah Kejadian</th>
                                                    <th colspan="2">Probabilitas</th>
                                                </tr>
                                                <tr>
                                                    <th>Keterangan</th>
                                                    <th>Menerima</th>
                                                    <th>Tidak Menerima</th>
                                                    <th>Menerima (C1)</th>
                                                    <th>Tidak Menerima (C0)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Ya</td>
                                                    <td>{{ $condPro['covid']['m_y'] }}</td>
                                                    <td>{{ $condPro['covid']['tm_y'] }}</td>
                                                    <td>{{ $condPro['covid']['p_m_y'] }}</td>
                                                    <td>{{ $condPro['covid']['p_tm_y'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tidak</td>
                                                    <td>{{ $condPro['covid']['m_t'] }}</td>
                                                    <td>{{ $condPro['covid']['tm_t'] }}</td>
                                                    <td>{{ $condPro['covid']['p_m_t'] }}</td>
                                                    <td>{{ $condPro['covid']['p_tm_t'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah</td>
                                                    <td>{{ $condPro['covid']['j_m'] }}</td>
                                                    <td>{{ $condPro['covid']['j_tm'] }}</td>
                                                    <td>{{ $condPro['covid']['j_p_m'] }}</td>
                                                    <td>{{ $condPro['covid']['j_p_tm'] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                                <section class="toggle">
                                    <label>Nilai Dari Lanjut Usia</label>
                                    <div class="toggle-content">
                                        <table class="table table-bordered table-striped mb-0">
                                            <thead style="text-align: center">
                                                <tr>
                                                    <th rowspan="">Lanjut Usia</th>
                                                    <th colspan="2">Jumlah Kejadian</th>
                                                    <th colspan="2">Probabilitas</th>
                                                </tr>
                                                <tr>
                                                    <th>Keterangan</th>
                                                    <th>Menerima</th>
                                                    <th>Tidak Menerima</th>
                                                    <th>Menerima (C1)</th>
                                                    <th>Tidak Menerima (C0)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Ya</td>
                                                    <td>{{ $condPro['lanjut_usia']['m_y'] }}</td>
                                                    <td>{{ $condPro['lanjut_usia']['tm_y'] }}</td>
                                                    <td>{{ $condPro['lanjut_usia']['p_m_y'] }}</td>
                                                    <td>{{ $condPro['lanjut_usia']['p_tm_y'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tidak</td>
                                                    <td>{{ $condPro['lanjut_usia']['m_t'] }}</td>
                                                    <td>{{ $condPro['lanjut_usia']['tm_t'] }}</td>
                                                    <td>{{ $condPro['lanjut_usia']['p_m_t'] }}</td>
                                                    <td>{{ $condPro['lanjut_usia']['p_tm_t'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah</td>
                                                    <td>{{ $condPro['lanjut_usia']['j_m'] }}</td>
                                                    <td>{{ $condPro['lanjut_usia']['j_tm'] }}</td>
                                                    <td>{{ $condPro['lanjut_usia']['j_p_m'] }}</td>
                                                    <td>{{ $condPro['lanjut_usia']['j_p_tm'] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(!empty($testing))
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="font-weight-bold text-dark">Perhitungan</h4>
                            <div class="toggle toggle-primary" data-plugin-toggle>
                                {{-- <section class="toggle">
                                    <label>Data Testing</label>
                                    <div class="toggle-content">
                                        <table class="table table-bordered table-striped mb-0">
                                            <thead style="text-align: center">
                                                <tr>
                                                    <th rowspan="2">No.</th>
                                                    <th rowspan="2">Nama</th>
                                                    <th rowspan="2">NIK</th>
                                                    <th style="text-align: center" colspan="6">Kriteria Calon Penerima BLT</th>
                                                    <th rowspan="2">Menerima BLT</th>
                                                </tr>
                                                <tr>
                                                    <td>Miskin Ekstrim</td>
                                                    <td>Kehilangan Mata Pencaharian</td>
                                                    <td>Sakit Menahun/Kronis</td>
                                                    <td>Keluarga yg belum terima JPS lainya</td>
                                                    <td>Keluarga Miskin Terdampak Covid-19</td>
                                                    <td>Anggota KK tunggal lanjut usia</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $no=1;    
                                                @endphp
                                                @foreach ($testing as $u)
                                                <tr style="text-align: center">                                    
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $u->nama_anggota_keluarga }}</td>
                                                    <td>{{ $u->nik }}</td>
                                                    <td>
                                                        @if ($u->miskin_ekstrim=='Ya')
                                                            <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                                        @endif
                                                    <td>
                                                        @if ($u->mata_pencaharian=='Ya')
                                                            <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($u->sakit_menahun=='Ya')
                                                            <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($u->keluarga_miskin=='Ya')
                                                            <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($u->covid=='Ya')
                                                            <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if ($u->lanjut_usia=='Ya')
                                                            <img src="{{ asset('logo/check.png') }}" width="50%" alt="">
                                                        @endif
                                                    </td>
                    
                                                    <td>{{ $u->blt }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </section> --}}
                            
                                <section class="toggle">
                                    <label>Perhitungan</label>
                                    <div class="toggle-content">
                                        <table class="table table-bordered mb-0">
                                            <thead style="text-align: center">
                                                <tr>
                                                    <th style="text-align: center" colspan="6">Hasil Proses Perhitungan</th>
                                                </tr>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Nama</th>
                                                    <th>NIK</th>
                                                    <th colspan="2">Proses</th>
                                                    <th>Hasil</th>
                                                </tr>
                                            
                                            </thead>
                                            <tbody>
                                                @php
                                                $no=1;  
                                                @endphp
                                                @foreach ($perhitungan as $p)
                                                <tr>
                                                    <td rowspan="2">{{ $no++ }}</td>
                                                    <td rowspan="2">{{ $p['nama_anggota_keluarga'] }}</td>
                                                    <td rowspan="2">{{ $p['nik'] }}</td>
                                                    <td >P(C1|X)=</td>
                                                    <td >{{ $p['c1'] }}</td>
                                                    <td rowspan="2">{{ $p['blt'] }}</td>
                                                </tr>
                                                <tr>
                                                    <td>P(C0|X)=</td>
                                                    <td>{{ $p['c0'] }}</td>
                                                    
                                                </tr>
                                                @endforeach
                                            
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if(!empty($klas))
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="font-weight-bold text-dark">Klasifikasi</h4>
                            <div class="toggle toggle-primary" data-plugin-toggle>
                                <section class="toggle">
                                    <label>Klasifikasi</label>
                                    <div class="toggle-content">
                                        <table class="table table-bordered table-striped mb-0">
                                            <thead style="text-align: center">
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Nama</th>
                                                    <th>NIK</th>
                                                    {{-- <th>Actual</th> --}}
                                                    <th>Predict</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $no=1;    
                                                @endphp
                                                @foreach ($klas as $u)
                                                <tr style="text-align: center">  
                                                    <td>{{ $no++ }}</td>                                  
                                                <td>{{ $u['nama_anggota_keluarga'] }}</td>
                                                <td>{{ $u['nik'] }}</td>
                                                <td>{{ $u['blt'] }}</td>
                                                {{-- <td>{{ $u['bltact'] }}</td> --}}
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                                {{-- <section class="toggle">
                                    <label>Confusion Matrix</label>
                                    <div class="toggle-content">
                                        <table class="table table-bordered mb-0">
                                            <thead style="text-align: center">
                                                <tr>
                                                    <th colspan="2" rowspan="2" >Klasifikasi</th>
                                                    <th colspan="2">Actual</th>
                                                </tr>
                                                <tr>
                                                    <th>Menerima</th>
                                                    <th>Tidak Menerima</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $no=1;    
                                                @endphp
                                                <tr>
                                                    <td rowspan="2" style="rotate: 270deg; text-align: center; font-weight: 700">Predict</td>
                                                    <td>Menerima</td>
                                                    <td>{{ $conf['tp'] }}</td>
                                                    <td>{{ $conf['fp'] }}</td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td>Tidak Menerima</td>
                                                    <td>{{ $conf['fn'] }}</td>
                                                    <td>{{ $conf['tn'] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </section> --}}
                            </div>
                        </div>
                    </div>
                    @endif

                    {{-- @if(!empty($hasil))
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="font-weight-bold text-dark">Hasil</h4>
                            <div class="toggle toggle-primary" data-plugin-toggle>

                                <section class="toggle">
                                    <label>Hasil</label>
                                    <div class="toggle-content">
                                        <table class="table table-bordered table-striped mb-0">
                                            <thead style="text-align: center">
                                                <tr>
                                                    <th>Data Tesing</th>
                                                    <th>Accuracy</th>
                                                    <th>Precision</th>
                                                    <th>Recall</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>{{ $persentase }} Data</td>
                                                    <td>{{ $hasil['acc'] }} %</td>
                                                    <td>{{ $hasil['pre'] }} %</td>
                                                    <td>{{ $hasil['rec'] }} %</td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    @endif --}}
                </div>
            </section>
        </div>
    </div>
<!-- end: page -->

<!-- Modal Animation -->
<div id="modalAnim" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Are you sure?</h2>
        </header>
        <form action="{{ route('del.uji.data.baru') }}" method="POST">
            @csrf
            <div class="card-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fas fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p class="mb-0">Apakah anda yakin menghapus data ini?</p>
                        <input type="hidden" id="delBanjar" name="id">
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-end">
                        <button type="submit" class="btn btn-primary">Confirm</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </form>
    </section>
</div>
<!-- Modal Animation -->

{{-- modal update --}}
<!-- Modal Form -->
<div id="editForm" class="modal-block modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Form Tambah {{ $title }}</h2>
        </header>
        <form method="POST" action="">
            @csrf
            <input type="hidden" name="id" id="editId">
            <div class="card-body">
                <div class="card-body"> 
                    <div class="form-row">
                        <label>No. KK :</label>
                        <div class="col-lg-12">
                            <select data-plugin-selectTwo class="form-control populate" id="nik" name="nik" ">
                                <option disabled value="">Pilih No. KK - NIK - Nama</option>
                                @foreach ($datakk as $i)
                                <option disabled {{ old('nik')== $i->nik ? 'selected':''}} value="{{ $i->nik }}">{{ $i->kode_keluarga .' - '.$i->nik .' - '. $i->nama_anggota_keluarga}}</option>
                                @endforeach
                            </select>
                      
                        </div>
                    </div>
                    <div class="form-row">
                        <label class=""">Checkboxes :</label>
                        <div class="col-lg-12">
                            <div class="checkbox-custom checkbox-default">
                                <input type="checkbox" id="checkboxExample1" class="miskin_ekstrim" value="Ya" name="miskin_ekstrim">
                                <label for="checkboxExample1">MISKIN EKSTRIM</label>
                            </div>

                            <div class="checkbox-custom checkbox-primary">
                                <input type="checkbox" id="checkboxExample2" class="mata_pencaharian" value="Ya" name="mata_pencaharian">
                                <label for="checkboxExample2">KEHILANGAN MATA PENCAHARIAN</label>
                            </div>

                            <div class="checkbox-custom checkbox-success">
                                <input type="checkbox" value="Ya" id="checkboxExample3" class="sakit_menahun" name="sakit_menahun">
                                <label for="checkboxExample3">SAKIT MENAHUN/KRONIS</label>
                            </div>

                            <div class="checkbox-custom checkbox-warning">
                                <input type="checkbox" value="Ya" id="checkboxExample4" class="keluarga_miskin" name="keluarga_miskin">
                                <label for="checkboxExample4">KELUARGA YG BELUM TERIMA JPS LAINNYA</label>
                            </div>

                            <div class="checkbox-custom checkbox-danger">
                                <input type="checkbox" value="Ya" id="checkboxExample5" class="covid" name="covid">
                                <label for="checkboxExample5">KELUARGA MISKIN TERDAMPAK COVID-19</label>
                            </div>
                            <div class="checkbox-custom">
                                <input type="checkbox" value="Ya" id="checkboxExample6" class="lanjut_usia" name="lanjut_usia">
                                <label for="checkboxExample6">ANGGOTA KK TUNGGAL LANJUT USIA</label>
                            </div>
                        </div>
                    </div>                    
                    
                </div>
                <footer class="card-footer">
                    <div class="row">
                        <div class="col-md-12 text-end">
                            <button type="submit" value="edit" name="edit" class="btn btn-primary">Submit</button>
                            <button class="btn btn-default modal-dismiss">Cancel</button>
                        </div>
                    </div>
                </footer>
            </div>
        </form>
    </section>
</div>

{{-- hapus semua data --}}
{{-- kosongkan data --}}

<div id="modalAnimDel" class="zoom-anim-dialog modal-block modal-block-primary mfp-hide">
    <section class="card">
        <header class="card-header">
            <h2 class="card-title">Konfirmasi</h2>
        </header>
        <form action="" method="POST">
            @csrf
            <div class="card-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fas fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <p class="mb-0">Apakah anda yakin mengkosongkan semua data ini?</p>
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-end">
                        <button type="submit" name="truncate" value="truncate" class="btn btn-primary">Confirm</button>
                        <button class="btn btn-default modal-dismiss">Cancel</button>
                    </div>
                </div>
            </footer>
        </form>
    </section>
</div>

<script>
    function editbtn(id){
        var input = document.getElementById("editId");
        input.value = id;

        // $('#editForm').modal('show');
        $.ajax({
            type: "GET",
            url: "/uji-data-baru-edit/"+id,
            success: function (response) {
                // console.log(id);
                $('#nik').val(response.training.nik).change();
                if(response.training.miskin_ekstrim =='Ya'){
                    $('.miskin_ekstrim').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.miskin_ekstrim').each(function(i) {
                        this.checked = false;
                    });
                }

                if(response.training.mata_pencaharian =='Ya'){
                    $('.mata_pencaharian').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.mata_pencaharian').each(function(i) {
                        this.checked = false;
                    });
                }

                if(response.training.sakit_menahun =='Ya'){
                    $('.sakit_menahun').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.sakit_menahun').each(function(i) {
                        this.checked = false;
                    });
                }

                if(response.training.keluarga_miskin =='Ya'){
                    $('.keluarga_miskin').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.keluarga_miskin').each(function(i) {
                        this.checked = false;
                    });
                }

                if(response.training.covid =='Ya'){
                    $('.covid').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.covid').each(function(i) {
                        this.checked = false;
                    });
                }

                if(response.training.lanjut_usia =='Ya'){
                    $('.lanjut_usia').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.lanjut_usia').each(function(i) {
                        this.checked = false;
                    });
                }

                if(response.training.blt =='Ya'){
                    $('.bltYa').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.bltTidak').each(function(i) {
                        this.checked = false;
                    });
                }


                
               
            }
        });
        // alert(id);
        // $('#modalUpdate').modal('show');
    }

   
</script>

<script>
$(function() {
    $(document).ready(function() {
        $("select.selectVal").change(function() {
            let selectedItem = $(this).children("option:selected").val();

            $.ajax({
            type: "GET",
            url: "/uji-data-baru-cek/"+ selectedItem,
            success: function (response) {
                console.log(response.cek);
                
                document.getElementById("kk").innerHTML=response.cek.kode_keluarga;
                document.getElementById("nama").innerHTML=response.cek.nama_anggota_keluarga;
                if(response.cek.usia>=60){
                    $('.lanjut_usia').each(function(i) {
                        this.checked = true;
                    });
                }else{
                    $('.lanjut_usia').each(function(i) {
                        this.checked = false;
                    });
                }
            }

            // alert("You have selected the name - " + selectedItem);
            // console.log(selectedItem);
          });
      });
    });
});
  </script>

{{-- Skrip tarik data modal confirmation --}}
<script>
    function del(id) {
        // $('#modal-category_name').html(id);
        var input = document.getElementById("delBanjar");
        input.value = id;

        
        // console.log(id);
        // $('#modal-confirm_delete').attr('onclick', 'confirmDelete(${id})');
        // $('#modalAnim').modal('show');
    }

    
</script>


@endsection

<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>



@push('js')
<!-- Specific Page Vendor -->
<script src="{{ asset('vendor/select2/js/select2.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
<script src="{{ asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js')}}"></script>

<script src="{{ asset('vendor/autosize/autosize.js')}}"></script>
<script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js')}}"></script>
@endpush

@push('example')

<!-- Examples -->
<script src="{{ asset('js/examples/examples.datatables.default.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.row.with.details.js')}}"></script>
<script src="{{ asset('js/examples/examples.datatables.tabletools.js')}}"></script>

<!-- Examples -->
<script>
(function($) {

'use strict';



$('.modal-with-move-anim').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: true,
    preloader: false,
    
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-slide-bottom',
    modal: true
});

/*
Modal Dismiss
*/
$(document).on('click', '.modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
});

/*
Modal Confirm
*/
$(document).on('click', '.modal-confirm', function (e) {
    e.preventDefault();
    $.magnificPopup.close();

    const del = document.querySelector("#delete");

    // console.log(del.dataset);
    // console.log('haii');
});

/*
Form
*/
$('.modal-with-form').magnificPopup({
    type: 'inline',
    preloader: false,
    focus: '#name',
    modal: true,

    // When elemened is focused, some mobile browsers in some cases zoom in
    // It looks not nice, so we disable it:
    callbacks: {
        beforeOpen: function() {
            if($(window).width() < 700) {
                this.st.focus = false;
            } else {
                this.st.focus = '#name';
            }
        }
    }
});

/*
Ajax
*/
$('.simple-ajax-modal').magnificPopup({
    type: 'ajax',
    modal: true
});

}).apply(this, [jQuery]);
</script>
@endpush