<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DataKKController;
use App\Http\Controllers\DataTrainingController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\UjiAkurasiController;
use App\Http\Controllers\UjiDataBaruController;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Route::middleware('auth')->group(function(){
//     Route::get('/dashboard',[HomeController::class,'index'])->name('dashboard');
// });

Route::get('/',function(){
    return view('auth');
});
Route::get('/login',[AuthController::class, 'login']);
Route::post('/login',[AuthController::class, 'login'])->name('login');


Route::group(['middleware' => ['auth']], function () {
    Route::get('logout',[AuthController::class, 'logout'])->name('logout');
    Route::get('dashboard',[HomeController::class,'index'])->name('dashboard');
    Route::get('profile',[HomeController::class,'profile']);

    Route::get('data-kk',[DataKKController::class,'index']);
    Route::post('data-kk',[DataKKController::class,'index']);
    Route::get('data-kk-edit/{id}',[DataKKController::class,'edit']);
    Route::post('data-kk-del',[DataKKController::class,'destroy'])->name('del.datakk');

    Route::get('data-kk-detail/{id}',[DataKKController::class,'detail']);
    Route::post('data-kk-detail/{id}',[DataKKController::class,'detail']);
    
    Route::get('data-training',[DataTrainingController::class,'index']);
    Route::post('data-training',[DataTrainingController::class,'index']);
    Route::get('data-training-edit/{id}',[DataTrainingController::class,'edit']);
    Route::post('data-training-del',[DataTrainingController::class,'destroy'])->name('del.data.training');
    
    Route::get('uji-akurasi',[UjiAkurasiController::class,'index']);
    Route::post('uji-akurasi',[UjiAkurasiController::class,'index']);
    
    Route::get('uji-data-baru',[UjiDataBaruController::class,'index']);
    Route::post('uji-data-baru',[UjiDataBaruController::class,'index']);
    Route::get('uji-data-baru-edit/{id}',[UjiDataBaruController::class,'edit']);
    Route::get('uji-data-baru-cek/{id}',[UjiDataBaruController::class,'cek']);
    Route::post('uji-data-baru-del',[UjiDataBaruController::class,'destroy'])->name('del.uji.data.baru');
    
    Route::get('laporan-data-blt',[LaporanController::class,'blt']);
    Route::get('laporan-data-spk',[LaporanController::class,'spk']);


    // Route::group(['middleware' => ['cekLogin:staff']], function () {

    //     Route::get('data-user',[DataUser::class,'index']);
    //     Route::post('data-user',[DataUser::class,'index']);
    // });

    
});

